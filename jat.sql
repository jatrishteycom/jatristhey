/**
 * User Login Table
 */

CREATE TABLE  `users` (
`user_id` INT NOT NULL primary key AUTO_INCREMENT ,
`username` VARCHAR(45) NULL unique,
`password` VARCHAR(45) NULL ,
 status enum('A','I','H','D') comment 'A=Active I=Inactive H=Hold D=Deleted',
accesstype enum('admin','user'),
usertype enum('P','G','N') comment 'P=Premium G=Gold N=Normal',
`email` VARCHAR(45) NULL ,
`token` VARCHAR(99) NULL ,
`token_expire` int(10) default 0 ,
`created` int(10) default 0 ,
`created_by` int(10) default 0,
`updated` int(10) default 0 ,
`updated_by` int(10) default 0
)ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;

/**
 * User Profile Tables 
 */

CREATE TABLE `user_profile` (
`profile_id` INT(10) NOT NULL primary key AUTO_INCREMENT ,
`user_id` INT(10) NOT NULL default 0 comment 'refers to users table',
`religion_id` INT(10) NOT NULL default 0 comment 'refers to religion table',
`gotra_id` INT(10) NOT NULL default 0 comment 'refers to gotra table',
`state_id` INT(10) NOT NULL default 0 comment 'refers to states table',
`cityid` INT(10) NOT NULL default 0 comment 'refers to cities table',
`country_id` INT(10) NOT NULL default 0 comment 'refers to countries table',
`district_id` INT(10) NOT NULL default 0 comment 'refers to dostricts table',
`fname` VARCHAR(32) NULL ,
`lname` VARCHAR(32) NULL ,
`gender` VARCHAR(1) NULL comment 'M=Male,F=Female',
`father_name` VARCHAR(50) NULL ,
`mother_name` VARCHAR(50) NULL ,
`refrence_name` VARCHAR(50) NULL ,
`refrence_number` VARCHAR(16) NULL ,
`mobile` VARCHAR(16) NULL ,
`dob` INT(10) default 0 ,
`created` int(10) default 0 ,
`created_by` int(10) default 0,
`updated` int(10) default 0 ,
`updated_by` int(10) default 0,
FOREIGN KEY (user_id) REFERENCES users(user_id),
FOREIGN KEY (religion_id) REFERENCES religion(id),
FOREIGN KEY (gotra_id) REFERENCES gotra(id),
FOREIGN KEY (state_id) REFERENCES states(state_id),
FOREIGN KEY (cityid) REFERENCES cities(city_id),
FOREIGN KEY (country_id) REFERENCES countries(country_id),
FOREIGN KEY (district_id) REFERENCES districts(district_id)
)ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;

CREATE TABLE profile_for (id int(5) NOT NULL AUTO_INCREMENT,  name varchar(50) DEFAULT NULL,primary key (id));
insert into profile_for(name)values('Myself');
insert into profile_for(name)values('Son');
insert into profile_for(name)values('Daughter');
insert into profile_for(name)values('Brother');
insert into profile_for(name)values('Sister');
insert into profile_for(name)values('Relative');
insert into profile_for(name)values('Friend');


/**
 * religion table
 */
	
drop table religion;
CREATE TABLE religion (id int(5) NOT NULL AUTO_INCREMENT,  name varchar(50) DEFAULT NULL,primary key (id));
insert into religion(name)values('Muslim');
insert into religion(name)values('Hindu');
insert into religion(name)values('Sikh');

CREATE TABLE gotra (id int(5) NOT NULL AUTO_INCREMENT,relegionrefid int(9), name varchar(50) DEFAULT NULL,primary key (id));
insert into gotra(relegionrefid,name) values(2,'Abusaria');
insert into gotra(relegionrefid,name) values(2,'Achara');
insert into gotra(relegionrefid,name) values(2,'Ahlawat');
insert into gotra(relegionrefid,name) values(2,'Agre');
insert into gotra(relegionrefid,name) values(2,'Ajmeria');
insert into gotra(relegionrefid,name) values(2,'Andhak');
insert into gotra(relegionrefid,name) values(2,'Anaadi');
insert into gotra(relegionrefid,name) values(2,'Antal');
insert into gotra(relegionrefid,name) values(2,'Asiagh');
insert into gotra(relegionrefid,name) values(2,'Atri');
insert into gotra(relegionrefid,name) values(2,'Atwal');
insert into gotra(relegionrefid,name) values(2,'Aulakh');
insert into gotra(relegionrefid,name) values(2,'Aujla');
insert into gotra(relegionrefid,name) values(2,'Agah');
insert into gotra(relegionrefid,name) values(2,'Babal');
insert into gotra(relegionrefid,name) values(2,'Bachhal');
insert into gotra(relegionrefid,name) values(2,'Badesha');
insert into gotra(relegionrefid,name) values(2,'Badyal');
insert into gotra(relegionrefid,name) values(2,'Bhatoa');
insert into gotra(relegionrefid,name) values(2,'Bagri');
insert into gotra(relegionrefid,name) values(2,'Bahia');
insert into gotra(relegionrefid,name) values(2,'Baht');
insert into gotra(relegionrefid,name) values(2,'Baidwan');
insert into gotra(relegionrefid,name) values(2,'Bains');
insert into gotra(relegionrefid,name) values(2,'Bajwa');
insert into gotra(relegionrefid,name) values(2,'Bajya');
insert into gotra(relegionrefid,name) values(2,'Balhara');
insert into gotra(relegionrefid,name) values(2,'Balyan');
insert into gotra(relegionrefid,name) values(2,'Bamraulia');
insert into gotra(relegionrefid,name) values(2,'Bana');
insert into gotra(relegionrefid,name) values(2,'Bansi');
insert into gotra(relegionrefid,name) values(2,'Barjati');
insert into gotra(relegionrefid,name) values(2,'Barola');
insert into gotra(relegionrefid,name) values(2,'Basra');
insert into gotra(relegionrefid,name) values(2,'Basran');
insert into gotra(relegionrefid,name) values(2,'Baswan');
insert into gotra(relegionrefid,name) values(2,'Bassi');
insert into gotra(relegionrefid,name) values(2,'Batar');
insert into gotra(relegionrefid,name) values(2,'Beniwal');
insert into gotra(relegionrefid,name) values(2,'Benning');
insert into gotra(relegionrefid,name) values(2,'Bhadare');
insert into gotra(relegionrefid,name) values(2,'Bhadiar');
insert into gotra(relegionrefid,name) values(2,'Bhadu');
insert into gotra(relegionrefid,name) values(2,'Bhalothia');
insert into gotra(relegionrefid,name) values(2,'Bhalli');
insert into gotra(relegionrefid,name) values(2,'Bhambu');
insert into gotra(relegionrefid,name) values(2,'Bhangu/Bhangoo');
insert into gotra(relegionrefid,name) values(2,'Bharhaich');
insert into gotra(relegionrefid,name) values(2,'Bhari');
insert into gotra(relegionrefid,name) values(2,'Bhela');
insert into gotra(relegionrefid,name) values(2,'Bhattal');
insert into gotra(relegionrefid,name) values(2,'Bhichar');
insert into gotra(relegionrefid,name) values(2,'Bhind');
insert into gotra(relegionrefid,name) values(2,'Bhukar');
insert into gotra(relegionrefid,name) values(2,'Bhullar');
insert into gotra(relegionrefid,name) values(2,'Billing');
insert into gotra(relegionrefid,name) values(2,'Brar');
insert into gotra(relegionrefid,name) values(2,'Braich');
insert into gotra(relegionrefid,name) values(2,'Budania');
insert into gotra(relegionrefid,name) values(2,'Budhwar');
insert into gotra(relegionrefid,name) values(2,'Burdak');
insert into gotra(relegionrefid,name) values(2,'Buttar');
insert into gotra(relegionrefid,name) values(2,'Chadhar');
insert into gotra(relegionrefid,name) values(2,'Chahal');
insert into gotra(relegionrefid,name) values(2,'Chahar');
insert into gotra(relegionrefid,name) values(2,'Chauhan');
insert into gotra(relegionrefid,name) values(2,'Chandel');
insert into gotra(relegionrefid,name) values(2,'Chikkara');
insert into gotra(relegionrefid,name) values(2,'Cheema');
insert into gotra(relegionrefid,name) values(2,'Chhillar');
insert into gotra(relegionrefid,name) values(2,'Chheena');
insert into gotra(relegionrefid,name) values(2,'Chaudhary');
insert into gotra(relegionrefid,name) values(2,'Chaitha');
insert into gotra(relegionrefid,name) values(2,'Dabas');
insert into gotra(relegionrefid,name) values(2,'Dabra');
insert into gotra(relegionrefid,name) values(2,'Dagur');
insert into gotra(relegionrefid,name) values(2,'Dahiya');
insert into gotra(relegionrefid,name) values(2,'Dandiwal');
insert into gotra(relegionrefid,name) values(2,'Dalal');
insert into gotra(relegionrefid,name) values(2,'Dangi');
insert into gotra(relegionrefid,name) values(2,'Deo');
insert into gotra(relegionrefid,name) values(2,'Deol');
insert into gotra(relegionrefid,name) values(2,'Deshwal');
insert into gotra(relegionrefid,name) values(2,'Dhariwal');
insert into gotra(relegionrefid,name) values(2,'Dhesi');
insert into gotra(relegionrefid,name) values(2,'Dhaliwal');
insert into gotra(relegionrefid,name) values(2,'Dhankhar');
insert into gotra(relegionrefid,name) values(2,'Dhadli');
insert into gotra(relegionrefid,name) values(2,'Dhanoa');
insert into gotra(relegionrefid,name) values(2,'Dhama');
insert into gotra(relegionrefid,name) values(2,'Dharan');
insert into gotra(relegionrefid,name) values(2,'Dharni');
insert into gotra(relegionrefid,name) values(2,'Dhatarwal');
insert into gotra(relegionrefid,name) values(2,'Dhatt');
insert into gotra(relegionrefid,name) values(2,'Dhaulya');
insert into gotra(relegionrefid,name) values(2,'Dhaurelia');
insert into gotra(relegionrefid,name) values(2,'Dhillon');
insert into gotra(relegionrefid,name) values(2,'Dhindawal');
insert into gotra(relegionrefid,name) values(2,'Dhindsa');
insert into gotra(relegionrefid,name) values(2,'Dholia');
insert into gotra(relegionrefid,name) values(2,'Dhoot');
insert into gotra(relegionrefid,name) values(2,'Dosanjh');
insert into gotra(relegionrefid,name) values(2,'Dudi');
insert into gotra(relegionrefid,name) values(2,'Duhan');
insert into gotra(relegionrefid,name) values(2,'Dullar');
insert into gotra(relegionrefid,name) values(2,'Fageria');
insert into gotra(relegionrefid,name) values(2,'Fandan');
insert into gotra(relegionrefid,name) values(2,'Farswal');
insert into gotra(relegionrefid,name) values(2,'Faugat');
insert into gotra(relegionrefid,name) values(2,'Faujdar');
insert into gotra(relegionrefid,name) values(2,'Garcha');
insert into gotra(relegionrefid,name) values(2,'Gahlot');
insert into gotra(relegionrefid,name) values(2,'Gandhar');
insert into gotra(relegionrefid,name) values(2,'Ghatwala');
insert into gotra(relegionrefid,name) values(2,'Garewal');
insert into gotra(relegionrefid,name) values(2,'Ghumman');
insert into gotra(relegionrefid,name) values(2,'Gill');
insert into gotra(relegionrefid,name) values(2,'Gauria');
insert into gotra(relegionrefid,name) values(2,'Gehlawat');
insert into gotra(relegionrefid,name) values(2,'Godara');
insert into gotra(relegionrefid,name) values(2,'Ghangas');
insert into gotra(relegionrefid,name) values(2,'Ghick');
insert into gotra(relegionrefid,name) values(2,'Gora');
insert into gotra(relegionrefid,name) values(2,'Goraya');
insert into gotra(relegionrefid,name) values(2,'Gosal');
insert into gotra(relegionrefid,name) values(2,'Grewal');
insert into gotra(relegionrefid,name) values(2,'Gulia');
insert into gotra(relegionrefid,name) values(2,'Guram');
insert into gotra(relegionrefid,name) values(2,'Gurm');
insert into gotra(relegionrefid,name) values(2,'Ghugh');
insert into gotra(relegionrefid,name) values(2,'Hala');
insert into gotra(relegionrefid,name) values(2,'Hanga');
insert into gotra(relegionrefid,name) values(2,'Hayer');
insert into gotra(relegionrefid,name) values(2,'Heer');
insert into gotra(relegionrefid,name) values(2,'Hooda');
insert into gotra(relegionrefid,name) values(2,'Hundal');
insert into gotra(relegionrefid,name) values(2,'Indolia');
insert into gotra(relegionrefid,name) values(2,'Jauhal');
insert into gotra(relegionrefid,name) values(2,'Jakhar');
insert into gotra(relegionrefid,name) values(2,'Jaglan');
insert into gotra(relegionrefid,name) values(2,'Janghu');
insert into gotra(relegionrefid,name) values(2,'Janu');
insert into gotra(relegionrefid,name) values(2,'Jatasra');
insert into gotra(relegionrefid,name) values(2,'Jatrana');
insert into gotra(relegionrefid,name) values(2,'Jatri');
insert into gotra(relegionrefid,name) values(2,'Jawanda');
insert into gotra(relegionrefid,name) values(2,'Jhajharia');
insert into gotra(relegionrefid,name) values(2,'Jhammat');
insert into gotra(relegionrefid,name) values(2,'Jhutti');
insert into gotra(relegionrefid,name) values(2,'Johal');
insert into gotra(relegionrefid,name) values(2,'Johiya');
insert into gotra(relegionrefid,name) values(2,'Joon');
insert into gotra(relegionrefid,name) values(2,'Jagpal');
insert into gotra(relegionrefid,name) values(2,'Jhinjar');
insert into gotra(relegionrefid,name) values(2,'Kahlon');
insert into gotra(relegionrefid,name) values(2,'Kadian');
insert into gotra(relegionrefid,name) values(2,'Kajala');
insert into gotra(relegionrefid,name) values(2,'Kakran');
insert into gotra(relegionrefid,name) values(2,'Kak');
insert into gotra(relegionrefid,name) values(2,'Kaler');
insert into gotra(relegionrefid,name) values(2,'Kalirai');
insert into gotra(relegionrefid,name) values(2,'Kalkat');
insert into gotra(relegionrefid,name) values(2,'Kalkhande');
insert into gotra(relegionrefid,name) values(2,'Kandhola');
insert into gotra(relegionrefid,name) values(2,'Kang');
insert into gotra(relegionrefid,name) values(2,'Karwasra');
insert into gotra(relegionrefid,name) values(2,'Kisana');
insert into gotra(relegionrefid,name) values(2,'Kaswan');
insert into gotra(relegionrefid,name) values(2,'Kataria');
insert into gotra(relegionrefid,name) values(2,'Katewa');
insert into gotra(relegionrefid,name) values(2,'Kehal');
insert into gotra(relegionrefid,name) values(2,'khangura');
insert into gotra(relegionrefid,name) values(2,'Khainwar');
insert into gotra(relegionrefid,name) values(2,'Khakh');
insert into gotra(relegionrefid,name) values(2,'Khalia');
insert into gotra(relegionrefid,name) values(2,'Kharb');
insert into gotra(relegionrefid,name) values(2,'Khatri');
insert into gotra(relegionrefid,name) values(2,'Khehra');
insert into gotra(relegionrefid,name) values(2,'Kherwa');
insert into gotra(relegionrefid,name) values(2,'Khichad');
insert into gotra(relegionrefid,name) values(2,'Khirwar');
insert into gotra(relegionrefid,name) values(2,'Khinger');
insert into gotra(relegionrefid,name) values(2,'Khokhar');
insert into gotra(relegionrefid,name) values(2,'Khosa');
insert into gotra(relegionrefid,name) values(2,'Khoye Maurya');
insert into gotra(relegionrefid,name) values(2,'Kooner');
insert into gotra(relegionrefid,name) values(2,'Kuhar');
insert into gotra(relegionrefid,name) values(2,'Kular');
insert into gotra(relegionrefid,name) values(2,'Kularia');
insert into gotra(relegionrefid,name) values(2,'Kulhari');
insert into gotra(relegionrefid,name) values(2,'Kundu');
insert into gotra(relegionrefid,name) values(2,'Kuntal');
insert into gotra(relegionrefid,name) values(2,'Lally');
insert into gotra(relegionrefid,name) values(2,'Lalli');
insert into gotra(relegionrefid,name) values(2,'Lakra');
insert into gotra(relegionrefid,name) values(2,'Lather');
insert into gotra(relegionrefid,name) values(2,'Langrial');
insert into gotra(relegionrefid,name) values(2,'Lakhlan');
insert into gotra(relegionrefid,name) values(2,'Lakhan');
insert into gotra(relegionrefid,name) values(2,'Lengha');
insert into gotra(relegionrefid,name) values(2,'Liddar');
insert into gotra(relegionrefid,name) values(2,'Lochab');
insert into gotra(relegionrefid,name) values(2,'Lathwal');
insert into gotra(relegionrefid,name) values(2,'Maan');
insert into gotra(relegionrefid,name) values(2,'Mandhan');
insert into gotra(relegionrefid,name) values(2,'Manes');
insert into gotra(relegionrefid,name) values(2,'Madrak');
insert into gotra(relegionrefid,name) values(2,'Mahal');
insert into gotra(relegionrefid,name) values(2,'Malik');
insert into gotra(relegionrefid,name) values(2,'Mandeer');
insert into gotra(relegionrefid,name) values(2,'Mander');
insert into gotra(relegionrefid,name) values(2,'Munder');
insert into gotra(relegionrefid,name) values(2,'Mandiwal');
insert into gotra(relegionrefid,name) values(2,'Mangat');
insert into gotra(relegionrefid,name) values(2,'Mann');
insert into gotra(relegionrefid,name) values(2,'Mundi');
insert into gotra(relegionrefid,name) values(2,'Mungut');
insert into gotra(relegionrefid,name) values(2,'Mede');
insert into gotra(relegionrefid,name) values(2,'Meel');
insert into gotra(relegionrefid,name) values(2,'Mehria');
insert into gotra(relegionrefid,name) values(2,'Maichu');
insert into gotra(relegionrefid,name) values(2,'Mohar');
insert into gotra(relegionrefid,name) values(2,'Monga');
insert into gotra(relegionrefid,name) values(2,'Moonga');
insert into gotra(relegionrefid,name) values(2,'Moond');
insert into gotra(relegionrefid,name) values(2,'Motsara');
insert into gotra(relegionrefid,name) values(2,'Mahawal');
insert into gotra(relegionrefid,name) values(2,'Maulia');
insert into gotra(relegionrefid,name) values(2,'Mahawalia');
insert into gotra(relegionrefid,name) values(2,'Mohil');
insert into gotra(relegionrefid,name) values(2,'Naga');
insert into gotra(relegionrefid,name) values(2,'Nagra');
insert into gotra(relegionrefid,name) values(2,'Nagauria');
insert into gotra(relegionrefid,name) values(2,'Nahl');
insert into gotra(relegionrefid,name) values(2,'Nain');
insert into gotra(relegionrefid,name) values(2,'Nandal');
insert into gotra(relegionrefid,name) values(2,'Nantaal');
insert into gotra(relegionrefid,name) values(2,'Natt');
insert into gotra(relegionrefid,name) values(2,'Nauhwar');
insert into gotra(relegionrefid,name) values(2,'Nehra');
insert into gotra(relegionrefid,name) values(2,'Nijjar');
insert into gotra(relegionrefid,name) values(2,'Nitharwal');
insert into gotra(relegionrefid,name) values(2,'Noon');
insert into gotra(relegionrefid,name) values(2,'Ohlan');
insert into gotra(relegionrefid,name) values(2,'Ola');
insert into gotra(relegionrefid,name) values(2,'Pachar');
insert into gotra(relegionrefid,name) values(2,'Pachehra');
insert into gotra(relegionrefid,name) values(2,'Padda');
insert into gotra(relegionrefid,name) values(2,'Palsania');
insert into gotra(relegionrefid,name) values(2,'Palrwal');
insert into gotra(relegionrefid,name) values(2,'Panaich');
insert into gotra(relegionrefid,name) values(2,'Panghal');
insert into gotra(relegionrefid,name) values(2,'Parihar');
insert into gotra(relegionrefid,name) values(2,'Pandher');
insert into gotra(relegionrefid,name) values(2,'Pangli');
insert into gotra(relegionrefid,name) values(2,'Pannu');
insert into gotra(relegionrefid,name) values(2,'Pansota');
insert into gotra(relegionrefid,name) values(2,'Pawar');
insert into gotra(relegionrefid,name) values(2,'Panwar');
insert into gotra(relegionrefid,name) values(2,'Phalaswal');
insert into gotra(relegionrefid,name) values(2,'Phogat');
insert into gotra(relegionrefid,name) values(2,'Pilania');
insert into gotra(relegionrefid,name) values(2,'Punia');
insert into gotra(relegionrefid,name) values(2,'Punial');
insert into gotra(relegionrefid,name) values(2,'Punian');
insert into gotra(relegionrefid,name) values(2,'Purwar');
insert into gotra(relegionrefid,name) values(2,'Purewal');
insert into gotra(relegionrefid,name) values(2,'Pooni');
insert into gotra(relegionrefid,name) values(2,'Poria');
insert into gotra(relegionrefid,name) values(2,'Potaysir');
insert into gotra(relegionrefid,name) values(2,'Rai');
insert into gotra(relegionrefid,name) values(2,'Rajawat');
insert into gotra(relegionrefid,name) values(2,'Rajian');
insert into gotra(relegionrefid,name) values(2,'Rajaura');
insert into gotra(relegionrefid,name) values(2,'Rana');
insert into gotra(relegionrefid,name) values(2,'Rakkar');
insert into gotra(relegionrefid,name) values(2,'Ranu');
insert into gotra(relegionrefid,name) values(2,'Ranwa');
insert into gotra(relegionrefid,name) values(2,'Rathi');
insert into gotra(relegionrefid,name) values(2,'Rasoda');
insert into gotra(relegionrefid,name) values(2,'Rawala');
insert into gotra(relegionrefid,name) values(2,'Rehal');
insert into gotra(relegionrefid,name) values(2,'Redhu');
insert into gotra(relegionrefid,name) values(2,'Repswal');
insert into gotra(relegionrefid,name) values(2,'Rhind-Tutt');
insert into gotra(relegionrefid,name) values(2,'Riar');
insert into gotra(relegionrefid,name) values(2,'Romana');
insert into gotra(relegionrefid,name) values(2,'Rulania');
insert into gotra(relegionrefid,name) values(2,'Rupal');
insert into gotra(relegionrefid,name) values(2,'Randhawa');
insert into gotra(relegionrefid,name) values(2,'Sabharwal');
insert into gotra(relegionrefid,name) values(2,'Sahota');
insert into gotra(relegionrefid,name) values(2,'Saharan');
insert into gotra(relegionrefid,name) values(2,'Samra');
insert into gotra(relegionrefid,name) values(2,'Sandhu');
insert into gotra(relegionrefid,name) values(2,'Sangwan');
insert into gotra(relegionrefid,name) values(2,'Sanghera');
insert into gotra(relegionrefid,name) values(2,'Saroha');
insert into gotra(relegionrefid,name) values(2,'Sran');
insert into gotra(relegionrefid,name) values(2,'Sra');
insert into gotra(relegionrefid,name) values(2,'Sehrawat');
insert into gotra(relegionrefid,name) values(2,'Seen');
insert into gotra(relegionrefid,name) values(2,'Sehwag');
insert into gotra(relegionrefid,name) values(2,'sejwal');
insert into gotra(relegionrefid,name) values(2,'Sekhon');
insert into gotra(relegionrefid,name) values(2,'Seoran');
insert into gotra(relegionrefid,name) values(2,'Sheoran');
insert into gotra(relegionrefid,name) values(2,'Shergill');
insert into gotra(relegionrefid,name) values(2,'Shokeen');
insert into gotra(relegionrefid,name) values(2,'Seokhand');
insert into gotra(relegionrefid,name) values(2,'Sidhu');
insert into gotra(relegionrefid,name) values(2,'Sigroha');
insert into gotra(relegionrefid,name) values(2,'Sikarwar');
insert into gotra(relegionrefid,name) values(2,'Sinsinwar');
insert into gotra(relegionrefid,name) values(2,'Sansanwal');
insert into gotra(relegionrefid,name) values(2,'Sirohi');
insert into gotra(relegionrefid,name) values(2,'SialSiwach');
insert into gotra(relegionrefid,name) values(2,'Sunda');
insert into gotra(relegionrefid,name) values(2,'Soban');
insert into gotra(relegionrefid,name) values(2,'Solanki');
insert into gotra(relegionrefid,name) values(2,'Sohal');
insert into gotra(relegionrefid,name) values(2,'Sohi');
insert into gotra(relegionrefid,name) values(2,'Sumal');
insert into gotra(relegionrefid,name) values(2,'Tharoda');
insert into gotra(relegionrefid,name) values(2,'Teerwal');
insert into gotra(relegionrefid,name) values(2,'Takhar');
insert into gotra(relegionrefid,name) values(2,'Tanwar');
insert into gotra(relegionrefid,name) values(2,'Tarar');
insert into gotra(relegionrefid,name) values(2,'Tatla');
insert into gotra(relegionrefid,name) values(2,'Tatran');
insert into gotra(relegionrefid,name) values(2,'Takshak');
insert into gotra(relegionrefid,name) values(2,'Tevatia');
insert into gotra(relegionrefid,name) values(2,'Thenua');
insert into gotra(relegionrefid,name) values(2,'Thakran');
insert into gotra(relegionrefid,name) values(2,'Thandi');
insert into gotra(relegionrefid,name) values(2,'Thathiala');
insert into gotra(relegionrefid,name) values(2,'Thind');
insert into gotra(relegionrefid,name) values(2,'Thori');
insert into gotra(relegionrefid,name) values(2,'Tiwana');
insert into gotra(relegionrefid,name) values(2,'Tokas');
insert into gotra(relegionrefid,name) values(2,'Tomara');
insert into gotra(relegionrefid,name) values(2,'Tomar');
insert into gotra(relegionrefid,name) values(2,'Toor');
insert into gotra(relegionrefid,name) values(2,'Tung');
insert into gotra(relegionrefid,name) values(2,'Tushir');
insert into gotra(relegionrefid,name) values(2,'Toot');
insert into gotra(relegionrefid,name) values(2,'Uppal');
insert into gotra(relegionrefid,name) values(2,'Udar');
insert into gotra(relegionrefid,name) values(2,'Ujjwal');
insert into gotra(relegionrefid,name) values(2,'Vanar');
insert into gotra(relegionrefid,name) values(2,'Virk');
insert into gotra(relegionrefid,name) values(2,'Vaince');
insert into gotra(relegionrefid,name) values(2,'Vijayrania');
insert into gotra(relegionrefid,name) values(2,'Wahla');
insert into gotra(relegionrefid,name) values(2,'Warraich');
insert into gotra(relegionrefid,name) values(2,'Waraich');
insert into gotra(relegionrefid,name) values(2,'Wainse');
/**
 * state table
 */
CREATE TABLE `states` (
  `state_id` int(10) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) NOT NULL DEFAULT '0',
  `State` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;;



INSERT INTO `states` VALUES (1,83,'Andaman and Nicobar'),(2,83,'Andhra Pradesh'),(3,83,'Arunachal Pradesh'),(4,83,'Assam'),(5,83,'Bihar'),(6,83,'Chandigarh'),(7,83,'Chhattisgarh'),(8,83,'Dadra and Nagar Haveli'),(9,83,'Daman and Diu'),(10,83,'Delhi'),(11,83,'Goa'),(12,83,'Gujarat'),(13,83,'Haryana'),(14,83,'Himachal Pradesh'),(15,83,'Jammu and Kashmir'),(16,83,'Jharkhand'),(17,83,'Karnataka'),(18,83,'Kerala'),(19,83,'Lakshadweep'),(20,83,'Madhya Pradesh'),(21,83,'Maharashtra'),(22,83,'Manipur'),(23,83,'Meghalaya'),(24,83,'Mizoram'),(25,83,'Nagaland'),(26,83,'Orissa'),(27,83,'Puducherry'),(28,83,'Punjab'),(29,83,'Rajasthan'),(30,83,'Sikkim'),(31,83,'Tamil Nadu'),(32,83,'Tripura'),(33,83,'Uttar Pradesh'),(34,83,'Uttarakhand'),(35,83,'West Benga'),(36,2,'Alabama'),(37,2,'Alaska'),(38,2,'Arizona'),(39,2,'Arkansas'),(40,2,'California'),(41,2,'Colorado'),(42,2,'Connecticut'),(43,2,'Delaware'),(44,2,'District of Columbia'),(45,2,'Florida'),(46,2,'Georgia'),(47,2,'Hawaii'),(48,2,'Idaho'),(49,2,'Illinois'),(50,2,'Indiana'),(51,2,'Iowa'),(52,2,'Kansas'),(53,2,'Kentucky'),(54,2,'Louisiana'),(55,2,'Maine'),(56,2,'Maryland'),(57,2,'Massachusetts'),(58,2,'Michigan'),(59,2,'Minnesota'),(60,2,'Mississippi'),(61,2,'Missouri'),(62,2,'Montana'),(63,2,'Nebraska'),(64,2,'Nevada'),(65,2,'New Hampshire'),(66,2,'New Jersey'),(67,2,'New Mexico'),(68,2,'New York'),(69,2,'North Carolina'),(70,2,'North Dakota'),(71,2,'Ohio'),(72,2,'Oklahoma'),(73,2,'Oregon'),(74,2,'Pennsylvania'),(75,2,'Rhode Island'),(76,2,''),(77,2,'South Carolina'),(78,2,'South Carolina'),(79,2,'South Dakota'),(80,2,'Tennessee'),(81,2,'Texas'),(82,2,'Utah'),(83,2,'Vermont'),(84,2,'Virginia'),(85,2,'Washington'),(86,2,'West Virginia'),(87,2,'Wisconsin'),(88,2,'Wyoming'),(89,3,'England'),(90,3,'Northern Ireland'),(91,3,'Scotland'),(92,3,'Wales'),(93,4,'Alberta'),(94,4,'British Columbia'),(95,4,'Labrado'),(96,4,'Manitoba'),(97,4,'New Brunswick'),(98,4,'Newfoundland'),(99,4,'Nova Scotia'),(100,4,'Ontario'),(101,4,'Prince Edward Island'),(102,4,'Quebec'),(103,4,'Saskatchewan'),(104,4,'ACT'),(105,5,'ACT'),(106,5,'New South Wales'),(107,5,'Northern Territory'),(108,5,'Queensland'),(109,5,'South Australia'),(110,5,'Tasmania'),(111,5,'Victoria'),(112,5,'Western Australia'),(113,6,'Albania'),(114,6,'Andorra'),(115,6,'Armenia'),(116,6,'Austria'),(117,6,'Belarus'),(118,6,'Belgium'),(119,6,'Bosnia'),(120,6,'Bulgaria'),(121,6,'Croatia'),(122,6,'Cyprus'),(123,6,'Czech Republic'),(124,6,'Denmark'),(125,6,'Estonia'),(126,6,'Faroe Islands'),(127,6,'Finland'),(128,6,'France'),(129,6,'Georgia'),(130,6,'Germany'),(131,6,'Gibraltar'),(132,6,'Greece'),(133,6,'Greenland'),(134,6,'Holland'),(135,6,'Hungary'),(136,6,'Iceland'),(137,6,'Ireland'),(138,6,'Italy'),(139,6,'Latvia'),(140,6,'Liechtenstein'),(141,6,'Lithuania'),(142,6,'Luxembourg'),(143,6,'Macedonia'),(144,6,'Malta'),(145,6,'Moldova'),(146,6,'Monaco'),(147,6,'Montenegro'),(148,6,'Norway'),(149,6,'Poland'),(150,6,'Portugal'),(151,6,'Romania'),(152,6,'Russia'),(153,6,'San Marino'),(154,6,'Serbia'),(155,6,'Slovakia'),(156,6,'Slovenia'),(157,6,'Spain'),(158,6,'Svalbard'),(159,6,'Sweden'),(160,6,'Switzerland'),(161,6,'Turkey'),(162,6,'Ukraine'),(163,6,'Vatican City'),(164,7,'Afghanistan'),(165,7,'Azerbaijan'),(166,7,'Bangladesh'),(167,7,'Bhutan'),(168,7,'Brunei'),(169,7,'Cambodia'),(170,7,'China'),(171,7,'Hong Kong'),(172,7,'Indonesia'),(173,7,'Japan'),(174,7,'Kazakhstan'),(175,7,'Kyrgyzstan'),(176,7,'Laos'),(177,7,'Macau'),(178,7,'Malaysia'),(179,7,'Maldives'),(180,7,'Mongolia'),(181,7,'Myanmar'),(182,7,'Nepal'),(183,7,'North Korea'),(184,7,'Pakistan'),(185,7,'Paracel Islands'),(186,7,'Philippines'),(187,7,'Singapore'),(188,7,'South Korea'),(189,7,'Spratly Islands'),(190,7,'Sri Lanka'),(191,7,'Taiwan'),(192,7,'Tajikistan'),(193,7,'Thailand'),(194,7,'Turkey'),(195,7,'Turkmenistan'),(196,7,'Uzbekistan'),(197,7,'Vietnam'),(198,8,'Bahrain'),(199,8,'Iran'),(200,8,'Iraq'),(201,8,'Israel'),(202,8,'Jordan'),(203,8,'Kuwait'),(204,8,'Lebanon'),(205,8,'Oman'),(206,8,'Qatar'),(207,8,'Saudi Arabia'),(208,8,'Syria'),(209,8,'UAE'),(210,8,'West Bank'),(211,8,'Yemen'),(212,9,'Algeria'),(213,9,'Angola'),(214,9,'Benin'),(215,9,'Botswana'),(216,9,'Burkina Faso'),(217,9,'Burundi'),(218,9,'Cameroon'),(219,9,'Cape Verde'),(220,9,'Cen. Afr. Rep.'),(221,9,'Chad'),(222,9,'Comoros'),(223,9,'Congo'),(224,9,'Djibouti'),(225,9,'Egypt'),(226,9,'Eq. Guinea'),(227,9,'Eritrea'),(228,9,'Ethiopia'),(229,9,'Gabon'),(230,9,'Ghana'),(231,9,'Guinea'),(232,9,'Guinea Bissau'),(233,9,'Ivory Coast'),(234,9,'Kenya'),(235,9,'Lesotho'),(236,9,'Liberia'),(237,9,'Libya'),(238,9,'Madagascar'),(239,9,'Malawi'),(240,9,'Mali'),(241,9,'Mauritania'),(242,9,'Mauritius'),(243,9,'Morocco'),(244,9,'Mozambique'),(245,9,'Namibia'),(246,9,'Niger'),(247,9,'Nigeria'),(248,9,'Reunion'),(249,9,'Rwanda'),(250,9,'Saint Helena'),(251,9,'Sao Tome'),(252,9,'Senegal'),(253,9,'Seychelles'),(254,9,'Sierra Leone'),(255,9,'Somalia'),(256,9,'South Africa'),(257,9,'Sudan'),(258,9,'Swaziland'),(259,9,'Tanzania'),(260,9,'The Gambia'),(261,9,'Togo'),(262,9,'Tunisia'),(263,9,''),(264,9,'Uganda'),(265,9,'W. Sahara'),(266,9,'Zaire'),(267,9,'Zambia'),(268,9,'Zimbabwe'),(269,10,'Anguilla'),(270,10,'Antigua'),(271,10,'Aruba'),(272,10,'Bahamas'),(273,10,'Barbados'),(274,10,'Belize'),(275,10,'Bermuda'),(276,10,'British V.I.'),(277,10,'Cayman Islands'),(278,10,'Colombia'),(279,10,'Costa Rica'),(280,10,'Cuba'),(281,10,'Dom. Republic'),(282,10,'Dominica'),(283,10,'Dutch Antilles'),(284,10,'El Salvador'),(285,10,'Grenada'),(286,10,'Grenadines'),(287,10,'Guadeloupe'),(288,10,'Haiti'),(289,10,'Jamaica'),(290,10,'Martinique'),(291,10,'Mexico'),(292,10,'Panama'),(293,10,'Puerto Rico'),(294,10,'St. Barths'),(295,10,'St. Kitts'),(296,10,'St. Lucia'),(297,10,'St. Martin'),(298,10,'St. Vincent'),(299,10,'Trinidad and Tobago'),(300,10,'Turks and Caicos'),(301,10,'US Virgin Islands'),(302,10,'Venezuela'),(303,11,'American Samoa'),(304,11,'Cook Islands'),(305,11,'East Timor'),(306,11,'Fiji'),(307,11,'French Polynesia'),(308,11,'Guam'),(309,11,'Kiribati'),(310,11,'Mariana Islands'),(311,11,'Marshall Islands'),(312,11,'Micronesia'),(313,11,'Nauru'),(314,11,'New Caledonia'),(315,11,'New Zealand'),(316,11,'Palau'),(317,11,'Pap. New Guinea'),(318,11,'Pitcairn Islands'),(319,11,'Samoa (Western)'),(320,11,'Solomon Isles'),(321,11,'Tokelau'),(322,11,'Tonga'),(323,11,'Tuvalu'),(324,11,'Vanuatu'),(325,11,'Wallis And Futuna'),(326,11,'Argentina'),(327,11,'Belize'),(328,12,'Belize'),(329,12,'Bolivia'),(330,12,'Brazil'),(331,12,'Chile'),(332,12,'Colombia'),(333,12,'Costa Rica'),(334,12,'Ecuador'),(335,12,'El Salvador'),(336,12,'Falklands'),(337,12,'Fr. Guiana'),(338,12,'Guatemala'),(339,12,'Guyana'),(340,12,'Honduras'),(341,12,'Mexico'),(342,12,'Nicaragua'),(343,12,'Panama'),(344,12,'Paraguay'),(345,12,'Peru'),(346,12,'Sth. Georgia'),(347,12,'Suriname'),(348,12,'Uruguay'),(349,12,'Venezuela');

CREATE TABLE `cities` (
  `city_id` int(10) NOT NULL AUTO_INCREMENT,
  `City` varchar(250) DEFAULT NULL,
  `stateid` int(5) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

insert into `cities`values('Delhi',1);

CREATE TABLE `countries` (
  `country_id` int(10) NOT NULL AUTO_INCREMENT,
  `Country` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


INSERT INTO `countries` VALUES (1,'Afghanistan'),(2,'Albania'),(3,'Algeria'),(4,'Andorra'),(5,'Angola'),(6,'Antigua and Barbuda'),(7,'Argentina'),(8,'Armenia'),(9,'Aruba'),(10,'Australia'),(11,'Austria'),(12,'Azerbaijan'),(13,'Bahamas, The'),(14,'Bahrain'),(15,'Bangladesh'),(16,'Barbados'),(17,'Belarus'),(18,'Belgium'),(19,'Belize'),(20,'Benin'),(21,'Bhutan'),(22,'Bolivia'),(23,'Bosnia and Herzegovina'),(24,'Botswana'),(25,'Brazil'),(26,'Brunei'),(27,'Bulgaria'),(28,'Burkina Faso'),(29,'Burma'),(30,'Burundi'),(31,'Cambodia'),(32,'Cameroon'),(33,'Canada'),(34,'Cabo Verde'),(35,'Central African Republic'),(36,'Chad'),(37,'Chile'),(38,'China'),(39,'Colombia'),(40,'Comoros'),(41,'Congo, Democratic Republic of the'),(42,'Congo, Republic of the'),(43,'Costa Rica'),(44,'Cote d\'Ivoire'),(45,'Croatia'),(46,'Cuba'),(47,'Curacao'),(48,'Cyprus'),(49,'Czechia'),(50,'Denmark'),(51,'Djibouti'),(52,'Dominica'),(53,'Dominican Republic'),(54,'East Timor (see Timor-Leste)'),(55,'Ecuador'),(56,'Egypt'),(57,'El Salvador'),(58,'Equatorial Guinea'),(59,'Eritrea'),(60,'Estonia'),(61,'Ethiopia'),(62,'Fiji'),(63,'Finland'),(64,'France'),(65,'Top of Page'),(66,'Gabon'),(67,'Gambia, The'),(68,'Georgia'),(69,'Germany'),(70,'Ghana'),(71,'Greece'),(72,'Grenada'),(73,'Guatemala'),(74,'Guinea'),(75,'Guinea-Bissau'),(76,'Guyana'),(77,'Haiti'),(78,'Holy See'),(79,'Honduras'),(80,'Hong Kong'),(81,'Hungary'),(82,'Iceland'),(83,'India'),(84,'Indonesia'),(85,'Iran'),(86,'Iraq'),(87,'Ireland'),(88,'Israel'),(89,'Italy'),(90,'Jamaica'),(91,'Japan'),(92,'Jordan'),(93,'Kazakhstan'),(94,'Kenya'),(95,'Kiribati'),(96,'Korea, North'),(97,'Korea, South'),(98,'Kosovo'),(99,'Kuwait'),(100,'Kyrgyzstan'),(101,'Laos'),(102,'Latvia'),(103,'Lebanon'),(104,'Lesotho'),(105,'Liberia'),(106,'Libya'),(107,'Liechtenstein'),(108,'Lithuania'),(109,'Luxembourg'),(110,'Macau'),(111,'Macedonia'),(112,'Madagascar'),(113,'Malawi'),(114,'Malaysia'),(115,'Maldives'),(116,'Mali'),(117,'Malta'),(118,'Marshall Islands'),(119,'Mauritania'),(120,'Mauritius'),(121,'Mexico'),(122,'Micronesia'),(123,'Moldova'),(124,'Monaco'),(125,'Mongolia'),(126,'Montenegro'),(127,'Morocco'),(128,'Mozambique'),(129,'Namibia'),(130,'Nauru'),(131,'Nepal'),(132,'Netherlands'),(133,'New Zealand'),(134,'Nicaragua'),(135,'Niger'),(136,'Nigeria'),(137,'North Korea'),(138,'Norway'),(139,'Oman'),(140,'Pakistan'),(141,'Palau'),(142,'Palestinian Territories'),(143,'Panama'),(144,'Papua New Guinea'),(145,'Paraguay'),(146,'Peru'),(147,'Philippines'),(148,'Poland'),(149,'Portugal'),(150,'Qatar'),(151,'Romania'),(152,'Russia'),(153,'Rwanda'),(154,'Saint Kitts and Nevis'),(155,'Saint Lucia'),(156,'Saint Vincent and the Grenadines'),(157,'Samoa'),(158,'San Marino'),(159,'Sao Tome and Principe'),(160,'Saudi Arabia'),(161,'Senegal'),(162,'Serbia'),(163,'Seychelles'),(164,'Sierra Leone'),(165,'Singapore'),(166,'Sint Maarten'),(167,'Slovakia'),(168,'Slovenia'),(169,'Solomon Islands'),(170,'Somalia'),(171,'South Africa'),(172,'South Korea'),(173,'South Sudan'),(174,'Spain'),(175,'Sri Lanka'),(176,'Sudan'),(177,'Suriname'),(178,'Swaziland'),(179,'Sweden'),(180,'Switzerland'),(181,'Syria'),(182,'Taiwan'),(183,'Tajikistan'),(184,'Tanzania'),(185,'Thailand'),(186,'Timor-Leste'),(187,'Togo'),(188,'Tonga'),(189,'Trinidad and Tobago'),(190,'Tunisia'),(191,'Turkey'),(192,'Turkmenistan'),(193,'Tuvalu'),(194,'Uganda'),(195,'Ukraine'),(196,'United Arab Emirates'),(197,'United Kingdom'),(198,'Uruguay'),(199,'Uzbekistan'),(200,'Vanuatu'),(201,'Venezuela'),(202,'Vietnam'),(203,'Yemen'),(204,'Zambia'),(205,'Zimbabwe');


CREATE TABLE `districts` (
  `district_id` int(10) NOT NULL AUTO_INCREMENT,
  `StateID` bigint(20) NOT NULL DEFAULT '0',
  `CountryID` bigint(20) NOT NULL DEFAULT '0',
  `District` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


ALTER TABLE `districts` CHANGE  `District` `name` VARCHAR(64) NOT NULL;

INSERT INTO districts(name,StateID) VALUES ('Saharanpur',33);
INSERT INTO districts(name,StateID) VALUES ('Muzaffarnagar',33);
INSERT INTO districts(name,StateID) VALUES ('Bulandshahar',33);
INSERT INTO districts(name,StateID) VALUES ('Ghaziabad',33);
INSERT INTO districts(name,StateID) VALUES ('Meerut',33);
mysql> ALTER TABLE user_profile DROP FOREIGN KEY `user_profile_ibfk_5`;
Query OK, 0 rows affected (0.30 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table user_profile drop column cityid;
Query OK, 0 rows affected (0.32 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> drop table cities;
Query OK, 0 rows affected (0.06 sec)

mysql> 
ALTER TABLE user_profile ADD mother_gotra INT(10) DEFAULT 0;
ALTER TABLE user_profile ADD reg_step INT(10) DEFAULT 0;
ALTER TABLE users ADD experi_time INT(10) DEFAULT 0;


CREATE TABLE `user_personal_info` (
`id` INT(10) NOT NULL primary key AUTO_INCREMENT ,
`user_id` INT(10) NOT NULL default 0 comment 'refers to users table',
`height` INT(10)  default 0 ,
`martitalstatus` INT(10) default 0,
`mother_tounge` INT(10) default 0,
`show_profile` INT(10) default 0,
`education` INT(10) default 0,
`occupation` INT(10) default 0,
`income` INT(10) default 0,
`eating` INT(10) default 0,
`drinking` INT(10) default 0,
`manglik` INT(10) default 0,
`created` int(10) default 0 ,
`created_by` int(10) default 0,
`updated` int(10) default 0 ,
`updated_by` int(10) default 0,
FOREIGN KEY (user_id) REFERENCES users(user_id)
)ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `ci_sessions` (
        `id` varchar(40) NOT NULL,
        `ip_address` varchar(45) NOT NULL,
        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
        `data` blob NOT NULL,
        KEY `ci_sessions_timestamp` (`timestamp`)
);
alter table countries add column deleted int (1) default 0;
update countries set deleted=1;
update countries set deleted=0 where Country='india';

alter table states add column deleted int (1) default 0;
update states set deleted=1;
update states set deleted=0 where state_id=33;
update states set deleted=0 where state_id=32;

CREATE TABLE `user_partner_info` (
`id` INT(10) NOT NULL primary key AUTO_INCREMENT ,
`user_id` INT(10) NOT NULL default 0 comment 'refers to users table',
`height_from` INT(10)  default 0 ,
`height_to` INT(10)  default 0 ,
`age_from` INT(10)  default 0 ,
`age_to` INT(10)  default 0 ,
`star` INT(10)  default 0 ,
`martitalstatus` INT(10) default 0,
`p_status` INT(10) default 0,
`mother_tounge` INT(10) default 0,
`education` INT(10) default 0,
`occupation` INT(10) default 0,
`religion` INT(10) default 0,
`income` INT(10) default 0,
`eating` INT(10) default 0,
`drinking` INT(10) default 0,
`manglik` INT(10) default 0,
`created` int(10) default 0 ,
`created_by` int(10) default 0,
`updated` int(10) default 0 ,
`updated_by` int(10) default 0,
FOREIGN KEY (user_id) REFERENCES users(user_id)
)ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;



CREATE TABLE `user_like` (
`id` INT(10) NOT NULL primary key AUTO_INCREMENT ,
`from_uid` INT(10) NOT NULL default 0 comment 'user who like',
`to_uid` INT(10)  default 0 comment 'user like to ',
`is_like` char(1)  default null,
`created` int(10) default 0 ,
`created_by` int(10) default 0,
`updated` int(10) default 0 ,
`updated_by` int(10) default 0
)ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;

CREATE TABLE `message` (
`id` INT(10) NOT NULL primary key AUTO_INCREMENT ,
`from_uid` INT(10) NOT NULL default 0 comment 'user who like',
`to_uid` INT(10)  default 0 comment 'user like to ',
`message` text  default null,
`deleted` char(1)  default 'N',
`created` int(10) default 0 ,
`created_by` int(10) default 0,
`updated` int(10) default 0 ,
`updated_by` int(10) default 0
)ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=latin1;


