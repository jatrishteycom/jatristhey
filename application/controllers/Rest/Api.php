<?php 
require_once("Restinc.php");
class Api extends REST {
	public function __construct() {
		parent::__construct();
		$this->inputs();
		
	}
	
	// Login Api will check user authentication and return token
	public function login(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('POST',$headers,FALSE);
		if(empty($headers[X_PASSWORD])){// check email key required
			$error = array(STATUS => FAIL, MESSAGE => "password field required");
				$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		$datavalue['email']=$headers[X_EMAIL];
		$datavalue['password']=md5($headers[X_PASSWORD]);
		
		$result=$this->Business_logic->getSingleTableData('users',$datavalue);#
		if(isset($result['0']['user_id']) && $result['0']['user_id']!= ''){
			$data1['experi_time']=time()+86400;
			$data1['token']=md5(time());
			$this->Business_logic->updateToTable('users','email',$datavalue['email'],$data1);
			
			$result=$this->Business_logic->getSingleTableData('users',$datavalue);#
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "Invalid username/password");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		
	}
	// change request API
	public function getProfileFor(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$result=$this->Business_logic->getSingleTableData('profile_for');
		if(count($result)>0){
		$responseresult = array(STATUS => SUCCESS, "result" => $result);
		$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		
		/*// validate headers is corrected or not
	
		$this->load->model('Changerequestapi'); //Change request models
		$tokenresult=$this->Changerequestapi->Validateusertoken($headers[X_EMAIL],$headers[X_TOKEN]);// validate user token
		if(empty($tokenresult['id'])){
			$this->getInvalidToken();//Display Invalid Toen Message
		}
		$result=$this->Changerequestapi->getImpactListData();
		$responseresult = array(STATUS => SUCCESS, "result" => $result);
		$this->Apilogic->response($this->Apilogic->json($responseresult), 200); */
	}
	// countries API
	public function getCountries(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$dataset['deleted']=0;
		$result=$this->Business_logic->getSingleTableData('countries',$dataset);
		if(count($result)>0){
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
		
	}
	
	// Religion API
	public function getReligion(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$result=$this->Business_logic->getSingleTableData('religion');
		if(count($result)>0){
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	
	}
	
	// Gotra API
	public function getGotra(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$religionid=$_GET['religionid'];
		if(empty($religionid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Religion cannot be empty');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		if(!is_numeric($religionid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Religion ID Sholud be numric only');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		$datavale['relegionrefid']=$religionid;
		
		$result=$this->Business_logic->getSingleTableData('gotra',$datavale);
		if(count($result)>0){
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	
	}
	
	// State API
	public function getState(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$countryid=$_GET['countryid'];
		if(empty($countryid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Country cannot be empty');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		if(!is_numeric($countryid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Country ID Sholud be numric only');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		$datavale['CountryID']=$countryid;
		$datavale['deleted']=0;
		$result=$this->Business_logic->getSingleTableData('states',$datavale);
		if(count($result)>0){
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	
	}
	
	// State API
	public function getCity(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$countryid=$_GET['stateid'];
		if(empty($countryid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Country cannot be empty');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		if(!is_numeric($countryid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Country ID Sholud be numric only');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		$datavale['stateid']=$countryid;
	
		$result=$this->Business_logic->getSingleTableData('cities',$datavale);
		if(count($result)>0){
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	
	}
	
	public function getDsitrict(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$countryid=$_GET['stateid'];
		if(empty($countryid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Country cannot be empty');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		if(!is_numeric($countryid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Country ID Sholud be numric only');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		$datavale['StateID']=$countryid;
	
		$result=$this->Business_logic->getSingleTableData('districts',$datavale);
		if(count($result)>0){
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	
	}
	
	/**
	 * Related Profile
	 */
	public function getRelatedProfile(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('GET',$headers);
		$profileid=$_GET['profileid'];
		if(empty($profileid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Invalid Profile Id');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		if(!is_numeric($profileid)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Profile ID Sholud be numric only');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		
		$dataset['email']=$headers[X_EMAIL];
		$dataset['status !=']='D';
		//$dataset['token']=$headers[X_TOKEN];
		$dataset['token']=$headers[X_TOKEN];
		$dataset['experi_time>']=time();
		$result=$this->Business_logic->getSingleTableData('users',$dataset);
		if(empty($result['0']['user_id'])){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Invalid Profile Id/ Or Toekn ID');
			$this->Apilogic->response($this->Apilogic->json($responseresult), 404);
		}
		$dataset1['user_id']=$result['0']['user_id'];
		$result1=$this->Business_logic->getSingleTableData('user_profile',$dataset1);
		
		$gender='M';
		$path="images/profile-image-men.jpg";
		if($result['0']['gender']=='M'){
			$gender='F';
			$path="images/profile-image-girl.jpg";
		}
		
		
		$page=$_GET['page'];
		if($page>0){
		    $page=$page*NUM_OF_RECORD_DASHBOARD-NUM_OF_RECORD_DASHBOARD;
		}else{
		    $page=0;
		}
		
		
		$query=$_GET['q'];
		if($_GET['quick_search']==1){
		    $gender=$_GET['g'];
		    $data=$this->Business_logic->getUserQuickSearchList("$gender",$_GET['age_from'],$_GET['age_to'],$_GET['religion'],$page,NUM_OF_RECORD_DASHBOARD);
		}elseif(!empty($query)){
		  $query_res = explode('=', $query);
		  $data=$this->Business_logic->getUserSearchList("$query_res[0]",$query_res[1],$page,NUM_OF_RECORD_DASHBOARD);#business logic data
		}else{
		    $data=$this->Business_logic->getRelatedUser("$gender",$page,NUM_OF_RECORD_DASHBOARD);#business logic data
		}
		
		foreach ($data['data'] as $res){
			$from = new DateTime(date('Y-m-d',$res->dob));
			$to   = new DateTime();
			#echo 
			
			$datavale['id']=$res->religion_id;
			$religion=$this->Business_logic->getSingleTableData('religion',$datavale);
			
			$datavale['id']=$res->gotra_id;
			$gotra=$this->Business_logic->getSingleTableData('gotra',$datavale);
			
			$datavale['id']=$res->education;
			$education=$this->Business_logic->getSingleTableData('education',$datavale);
			
			$datavale['id']=$res->occupation;
			$occupation=$this->Business_logic->getSingleTableData('occupation',$datavale);
			
			$datavale['id']=$res->height;
			$height=$this->Business_logic->getSingleTableData('heightgroombride',$datavale);
			
			$datavale['id']=$res->income;
			$income=$this->Business_logic->getSingleTableData('annualincome',$datavale);
			
			$profileimg['user_id']=$res->user_id;
			$img=$this->Business_logic->getSingleTableData('user_profile_image',$profileimg,0,1,'id','desc');
			if(!empty($img['0']['name'])){
				$path="profile/".$img['0']['name'];
			}
			
			
			$dataprofile['id']="JAT$gender".$res->user_id;
			$dataprofile['uid']=$res->user_id;
			$dataprofile['age']=$from->diff($to)->y;
			$dataprofile['religion']=$religion[0]['name'];
			$dataprofile['gotre']=$gotra[0]['name'];
			$dataprofile['education']=$education[0]['name'];
			$dataprofile['profession']=$occupation['0']['name'];
			$dataprofile['salary']=$income['0']['name'];
			$dataprofile['gender']=$gender;
			$dataprofile['path']=$path;
			$dataprofile['height']=$height['0']['name'];
			$maindata[]=$dataprofile;
		}
		#$result=$this->Business_logic->getSingleTableData('districts',$datavale);
		if(count($maindata)>0){
		    $responseresult = array(STATUS => SUCCESS, "result" => $maindata,"count"=>$data['count']);
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		}
		else{
			$error = array(STATUS => FAIL, MESSAGE => "No data Found");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	
	}
	
	// Login Api will check user authentication and return token
	/* public function login(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('POST',$headers,false);
		if(empty($headers[X_PASSWORD])){// check email key required
			$error = array(STATUS => FAIL, MESSAGE => "password field required");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		$response =$this->Apilogic->loginsubmit($headers[X_EMAIL],$headers[X_PASSWORD]);// login reuslt 
	} */
	/**
	 * Validate header section
	 * @param string $method
	 * @param unknown $headers
	 * @param string $tokencheck
	 */
	function getValidateHeader($method1='GET',$headers,$tokencheck=TRUE){
		$method=$this->get_request_method();
		if($method != $method1){ // check post method exist or not
			$error = array(STATUS => FAIL, MESSAGE => "Method not allow");
			$this->Apilogic->response($this->Apilogic->json($error), 405);
		}
		
		if($headers['Content-Type'] != "application/json"){ // check post method exist or not
			$error = array(STATUS => FAIL, MESSAGE => "Only application/json content type allow");
			$this->Apilogic->response($this->Apilogic->json($error), 406);
		}
		
		if(empty($headers[X_EMAIL])){// check email key required
			$error = array(STATUS => FAIL, MESSAGE => "email id required");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		
		if($tokencheck){
			if(empty($headers[X_TOKEN])){// check email key required
				$error = array(STATUS => FAIL, MESSAGE => "Token field required");
				$this->Apilogic->response($this->Apilogic->json($error), 404);
			}
		}
	
		
	}
	
public function Registratiostep1Submit(){
	$headers = apache_request_headers(); // fetch heaer value
	$this->getValidateHeader('POST',$headers,FALSE);
	$response=file_get_contents('php://input');
	$response1=json_decode($response);// fetch all input value
	if(! isset($response1->email)){// check email key required
	$error = array(STATUS => FAIL, MESSAGE => "email  key not found");
	$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	if(empty($response1->email)){// check email id should not be empty
	$error = array(STATUS => FAIL, MESSAGE => "Email  id  required");
	$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	if(empty($response1->password)){// check email id should not be empty
		$error = array(STATUS => FAIL, MESSAGE => "Password required");
		$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	
	if(empty($response1->mobile)){// check email id should not be empty
		$error = array(STATUS => FAIL, MESSAGE => "Mobile Number required");
		$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	
	if(empty($response1->dob)){// check email id should not be empty
		$error = array(STATUS => FAIL, MESSAGE => "dob required");
		$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	$data['email']=$response1->email;
	$userdata=$this->Business_logic->getSingleTableData('users',$data);
		if(!empty($userdata['0']['user_id'])){
		$error = array(STATUS => FAIL, MESSAGE => "Email Id allready exist in our database");
		$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	$data1['username']=$response1->mobile;
	$userdata=$this->Business_logic->getSingleTableData('users',$data1);
	if(!empty($userdata['0']['user_id'])){
		$error = array(STATUS => FAIL, MESSAGE => "Mobile Number  allready exist in our database");
		$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	$options = [
	'cost' => 12,
	];
	
	$savedata=array();
	$savedata['username']=$response1->mobile;
	$savedata['password']=md5($response1->password);;
	$savedata['status']='H';
	$savedata['accesstype']='user';
	$savedata['usertype']='P';
	$savedata['token']=md5(time());
	$savedata['created']=time();
	$savedata['created_by']=$response1->email;
	$savedata['email']=$response1->email;
	$savedata['experi_time']=time()+86400;
	$password=$savedata['password'];
	$savedid=$this->Business_logic->addToTable('users',$savedata);#Save Data into table
	//$savedid=1;
	$savedata=array();
	$savedata['user_id']=$savedid;
	$savedata['religion_id']=$response1->religion_id;
	$savedata['gotra_id']=$response1->gotra_id;;
	$savedata['mother_gotra']=$response1->mother_gotra;;
	$savedata['state_id']=$response1->state_id;;
	$savedata['country_id']=$response1->country_id;
	$savedata['district_id']=$response1->district_id;
	$savedata['fname']=$response1->fname;
	$savedata['lname']=$response1->lname;
	$savedata['gender']=$response1->gender;
	$savedata['reg_step']=1;
	$savedata['father_name']=$response1->father_name;
	$savedata['mother_name']=$response1->mother_name;
	$savedata['refrence_name']=$response1->refrence_name;
	$savedata['refrence_number']=$response1->refrence_number;
	$savedata['mobile']=$response1->mobile;
	$savedata['dob']=strtotime($response1->dob);
	$savedata['created']=time();
	$savedata['dob']=strtotime($response1->dob);
	$savedata['created']=time();
	$savedata['created_by']=$savedid;
	
	
	
	//$savedata['email']=$response1->email;
	$this->Business_logic->addToTable('user_profile',$savedata);
	if(!empty($savedid)){
		$datavalue['email']=$response1->email;
		$datavalue['password']=$password;
		$result=$this->Business_logic->getSingleTableData('users',$datavalue);#
	#	$responseresult = array(STATUS => SUCCESS, "result" => $result);
	    $responseresult = array(STATUS => SUCCESS, MESSAGE => "Account created", "result" => $result);
		$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
		//$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
	
	else{
		$error = array(STATUS => FAIL, MESSAGE => "No data Found");
		$this->Apilogic->response($this->Apilogic->json($error), 404);
	}
//	$response =$this->Apilogic->Validateusertoken($response1->email,$response1->token);// token authentication
	}
	
	/**
	 * Like Profile
	 */
	public function LikeProfile(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('POST',$headers,TRUE);
		$response=file_get_contents('php://input');
		$response1=json_decode($response);// fetch all input value
		
	
		if(empty($response1->from_uid)){// check email id should not be empty
			$error = array(STATUS => FAIL, MESSAGE => "From Uid Required");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
	if(empty($response1->to_uid)){// check email id should not be empty
			$error = array(STATUS => FAIL, MESSAGE => "To Uid Required");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
		$data['to_uid']=$response1->to_uid;
		$data['from_uid']=$response1->from_uid;
		$userdata=$this->Business_logic->getSingleTableData('user_like',$data);
		if(!empty($userdata['0']['id'])){
			$postdata['is_like']=$response1->is_like;
			$postdata['updated']=time();
			$postdata['updated_by']=$response1->from_uid;
			$status=$this->Business_logic->updateTablewithMulticond('user_like',$data,$postdata);
		}
		else {
			$postdata['created']=time();
			$postdata['created_by']=$response1->from_uid;
			$postdata['is_like']=$response1->is_like;
			$postdata['to_uid']=$response1->to_uid;
			$postdata['from_uid']=$response1->from_uid;
			$status=$this->Business_logic->addToTable('user_like',$postdata);
		}
		
		
		if(!empty($status)){
			$responseresult = array(STATUS => SUCCESS, MESSAGE => "Action Taken");
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
			//$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
		else{
			$error = array(STATUS => FAIL, MESSAGE => "Try Again");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		//	$response =$this->Apilogic->Validateusertoken($response1->email,$response1->token);// token authentication
	}
	
	/**
	 * Like Profile
	 */
	public function SendMessage(){
		$headers = apache_request_headers(); // fetch heaer value
		$this->getValidateHeader('POST',$headers,TRUE);
		$response=file_get_contents('php://input');
		$response1=json_decode($response);// fetch all input value
	
	
		if(empty($response1->from_uid)){// check email id should not be empty
			$error = array(STATUS => FAIL, MESSAGE => "From Uid Required");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
		if(empty($response1->to_uid)){// check email id should not be empty
			$error = array(STATUS => FAIL, MESSAGE => "To Uid Required");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
		$data['to_uid']=$response1->to_uid;
		$data['from_uid']=$response1->from_uid;
		$userdata=$this->Business_logic->getSingleTableData('message',$data);
		
			$postdata['created']=time();
			$postdata['created_by']=$response1->from_uid;
			$postdata['message']=$response1->message;
			$postdata['to_uid']=$response1->to_uid;
			$postdata['from_uid']=$response1->from_uid;
			$status=$this->Business_logic->addToTable('message',$postdata);
	
		if(!empty($status)){
			$responseresult = array(STATUS => SUCCESS, MESSAGE => "Action Taken");
			$this->Apilogic->response($this->Apilogic->json($responseresult), 200);
			//$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
	
		else{
			$error = array(STATUS => FAIL, MESSAGE => "Try Again");
			$this->Apilogic->response($this->Apilogic->json($error), 404);
		}
		//	$response =$this->Apilogic->Validateusertoken($response1->email,$response1->token);// token authentication
	}

}


?>