<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
	
	// load feedback form
	public function feed(){
		$this->load->view('/template/support/feedback');
	}
	
	// load Contact about us form
	public function about(){
		$this->load->view('/template/support/about');
	}
	
	// load Contact us form
	public function contact(){
		$this->load->view('/template/support/contact');
	}	
}