<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myprofile extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        
    }
    public function userprofile($id=''){
    	$id=$this->uri->segment('3');
    	if(!$this->Business_logic->is_logged_in()){
    	    
    	   $this->session->set_userdata(array('loginmessage' => 'Please Login First'));
    		redirect('/home', 'refresh');
    	}
    	$data['res']=$this->Business_logic->getRelatedUser('',0,1,$id,'asc',FALSE)['0'];
    	
    	$this->load->view('template/profiledetails/profiledetails',$data);
    }
    
}
?>