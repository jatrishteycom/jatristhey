<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
	    if($this->Business_logic->is_logged_in()){
	    	$this->Business_logic->getUserRegstep();
	      #  redirect('/registration/step2', 'refresh');
	    }
	    
	    /// comment below given link pls ===================>>	DEVENDRA	    
	    
	  //  $this->load->view('template/profiledetails/profiledetails'); #JUST ADDED A PROFILE PAGE
	    
	    
	  //  print_r($_SESSION);
	   // $this->session->set_userdata(array('message' => 'testing session'));
	//  $this->load->view('template/home/homepage'); #home page acutal page
	   // $this->session->set_userdata(array('loginmessage' => 'Please Enter User Name'));
		#$this->load->view('template/regstpahead'); #home page
		//$this->load->view('template/support/feedback'); #feedback
	    
	 //  $this->load->view('template/userhome'); # user landing home page
	  $this->load->view('template/home/homepage'); #home page acutal page
	
}
	
	public function registration_step1(){
	    if($this->Business_logic->is_logged_in()){
	        redirect('/registration/step2', 'refresh');
	    }
	    if($_REQUEST['email']){
	    $email =trim($_REQUEST['email']);
	    $password = trim($_REQUEST['password']);
	    $mobile = trim($_REQUEST['mobile']);
	    $dob = trim($_REQUEST['dob']);
	    $religion_id = trim($_REQUEST['religion_id']);
	    $gotra_id = trim($_REQUEST['gotra_id']);
	    $mother_gotra = trim($_REQUEST['mother_gotra']);
	    $state_id = trim($_REQUEST['state_id']);
	    $country_id = trim($_REQUEST['country_id']);
	    $district_id = trim($_REQUEST['district_id']);
	    $fname = trim($_REQUEST['fname']);
	    $lname = trim($_REQUEST['lname']);
	    $gender = trim($_REQUEST['gender']);
	    
	    /*
	    if (empty($fname)) {
	      $this->session->set_userdata(array('message' => 'First Name is required'));
	    } elseif (!preg_match("/^[a-zA-Z ]*$/",$fname)) {
	        $_SESSION['message'][] = "Only letters and white space allowed";
	    }
	    if (empty($lname)) {
	        $_SESSION['message'][] = "Last Name is required";
	    } elseif (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
	        $_SESSION['message'][] = "Only letters and white space allowed";
	    }
	    if (empty($gender)) {
	        $_SESSION['message'][] = "Gender is required";
	    }
	    if (empty($mobile)) {
	        $_SESSION['message'][] = "Mobile is required";
	    } elseif(!preg_match('/^[0-9]{10}+$/', $mobile)){
	        $_SESSION['message'][] = "Only letters and white space allowed";
	    }
	    
	    if (empty($_POST["email"])) {
	        $_SESSION['message'][] = "Email is required";
	    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	        $_SESSION['message'][] = "Invalid email format";
	    }
	    */
	   
	    $data_result=["email"=>$email,"password"=>$password,"mobile"=>$mobile,"dob"=>$dob,"religion_id"=>$religion_id,
	        "gotra_id"=>$gotra_id,"mother_gotra"=>$mother_gotra,"state_id"=>$state_id,"country_id"=>$country_id,
	        "district_id"=>$district_id,"fname"=>$fname,"lname"=>$lname,"gender"=>$gender
	    ];
	    
	   
	    
	    $url=base_url().ADD_STEP1;
	    $ch = curl_init($url);
	    $data = json_encode($data_result);
	  
	    curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
	    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	    curl_setopt($ch, CURLOPT_PORT, 80);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                	        'X-Email:abc@gmail.com',
                	        'X-Token:f85307a7c73640e75cfb9af4a325f5d1f3c1583d9916cf2565a0dd2e5919a6f7',
                	        'Content-Type:application/json',
	                    )
	    );
	    $response = curl_exec($ch);
	    curl_close($ch);
	   // echo $response;
	    $response=json_decode($response,true);
	    #print_r($response);exit;
	    
	    if($response['status']==0){
	        $this->session->set_userdata(array('message' => $response['message']));
	        redirect('/home', 'refresh');
	       
	    }else{
	        /* $this->session->set_userdata(array('username' => $email));
	        $this->session->set_userdata(array('message' => $response['message']));
	        redirect('/registration/step2', 'refresh'); */

	        $this->Business_logic->setSession($response['result'][0]);
	        //$this->session->set_userdata(array('username' => $email));
	        //$this->session->set_userdata(array('message' => $response['message']));
	        redirect('registration/step2');

	    }
	    
	}
	
}
	
	public function login_user(){
	    
	    if($_REQUEST['email']){
	       // print_r($_REQUEST);exit;
	        
	        $email = $_REQUEST['email'];
	        $password = $_REQUEST['password'];
	        $data_result=["email"=>$email,"password"=>$password];
	        
	      //  $data_result=["email"=>$email,"password"=>$password];
	        $url=base_url().LOGIN;
	        $ch = curl_init($url);
	       // $data = json_encode($data_result);
	        //curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($ch, CURLOPT_PORT, 80);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	            'X-Email:'.$email,
	            'X-Password:'.$password,
	            'X-Token:f85307a7c73640e75cfb9af4a325f5d1f3c1583d9916cf2565a0dd2e5919a6f7',
	            'Content-Type:application/json',
	        )
	            );
	        $response = curl_exec($ch);
	        curl_close($ch);
	        //echo $response;exit;
	        $response=json_decode($response,true);
	       
	     
	      
	        if($response['status']==0){
	            $this->session->set_userdata(array('loginmessage' => $response['message']));
	            redirect('/home');
	        }else{

	            /* $this->Business_logic->setSession($response['result']);
	            
	            $username = $this->session->userdata('email');
	            $usertocken = $this->session->userdata('token');
	            $user_expirytime = $this->session->userdata('experi_time'); */
	          

	          //  print_r($response['result'][0]);
	            $this->Business_logic->setSession($response['result'][0]);
	           
	           $user_profile=$this->Business_logic->getSingleTableData('user_profile',array('user_id'=>$response['result'][0]['user_id']));
	           	$this->Business_logic->getUserRegstep($user_profile[0]['reg_step']);
	           
	        }
	    }else{
	        redirect('/login', 'refresh');
	    }	    
	        $this->session->set_userdata(array('loginmessage' => 'Please Enter User Name'));
	        redirect('/home');
	    }
	
	public function logout(){
	    $this->session->sess_destroy();
	    session_destroy();
	    redirect('/home', 'refresh');
	}
	
	
	public function registration_step2(){
	   // print_r($_SESSION);
	    if(!$this->Business_logic->is_logged_in()){
	        redirect('/home', 'refresh');
	    }
	    $this->load->view('template/regstpahead');
	    
	}
	
	public function registration_step2_submit(){
		if(!$this->Business_logic->is_logged_in()){
			redirect('/home', 'refresh');
		}
	    $this->load->library('form_validation');
	  //  $this->load->library('image_lib');
	//	echo "jheerre";exit;
		$height=$this->input->post('height');
		$martitalstatus=$this->input->post('martitalstatus');
		$mother_tounge=$this->input->post('mother_tounge');
		$show_profile=$this->input->post('show_profile');
		$education=$this->input->post('education');
		$occupation=$this->input->post('occupation');
		$income=$this->input->post('income');
		$eating=$this->input->post('eating');
		$drinking=$this->input->post('drinking');
		$manglik=$this->input->post('manglik');
		$savedid=  $this->session->userdata('user_id');;
		$m = $_FILES['userfile1']['name'];
		$n = $_FILES['userfile2']['name']; 
		if ($m !== ""){
    		$this->form_validation->set_rules('userfile1', 'Profile Image', 'callback_image_upload');
    		$config = array(
    		    'upload_path' => "./assets/profile/",
    		    'allowed_types' => "jpg|png|jpeg",
    		    'overwrite' => TRUE,
    		    'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
    		);
    		$this->load->library('upload', $config);
    		if(!$this->upload->do_upload('userfile1'))
    		{
    		    $this->form_validation->set_rules('userfile1', $this->upload->display_errors());
    		    $this->session->set_userdata(array('step2form_msg' => $this->upload->display_errors()));
    		    //$this->load->view('template/regstpahead');
    		    redirect('/registration/step2', 'refresh');
    		}else{
    		    $data3 = $this->upload->data(); 
    		    $config['image_library'] = 'gd2';
    		    $config['source_image'] = './assets/profile/'.$data3["file_name"];  
    		    //$config['create_thumb'] = TRUE;
    		    $config['maintain_ratio'] = FALSE;
    		    $config['quality'] = '60%';
    		    $config['width'] = 200;
    		    $config['height'] = 200;
    		    $config['new_image'] = './assets/profile/'.$data3["file_name"];
    		    $this->load->library('image_lib', $config);
    		    if ( !$this->image_lib->resize())
    		    {
    		       // echo $this->image_lib->display_errors();exit;
    		        $this->form_validation->set_rules('userfile1', $this->image_lib->display_errors());
    		        $this->session->set_userdata(array('step2form_msg' => $this->image_lib->display_errors()));
    		        //$this->load->view('template/regstpahead');
    		        redirect('/registration/step2', 'refresh');
    		    }
    		    
    		    $dataimage=array();
    		    $dataimage['user_id']=$savedid;
    		    $dataimage['name']=$this->upload->data('file_name');
    		    $this->Business_logic->addToTable('user_profile_image',$dataimage);
    		}
		}
		if ($n !== ""){
		    $this->form_validation->set_rules('userfile2', 'Profile Image', 'callback_image_upload');
		    $config2 = array(
		        'upload_path' => "./assets/profile/",
		        'allowed_types' => "jpg|png|jpeg",
		        'overwrite' => TRUE,
		        'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
		    );
		    $this->load->library('upload', $config2);
		    if(!$this->upload->do_upload('userfile2'))
		    {
		        $this->form_validation->set_rules('userfile2', $this->upload->display_errors());
		        $this->session->set_userdata(array('step2form_msg' => $this->upload->display_errors()));
		       // $this->load->view('template/regstpahead');
		        redirect('/registration/step2', 'refresh');
		    }else{
		        $data3 = $this->upload->data();
		        $config2['image_library'] = 'gd2';
		        $config2['source_image'] = './assets/profile/'.$data3["file_name"];
		        //$config['create_thumb'] = TRUE;
		        $config2['maintain_ratio'] = FALSE;
		        $config2['quality'] = '60%';
		        $config2['width'] = 200;
		        $config2['height'] = 200;
		        $config2['new_image'] = './assets/profile/'.$data3["file_name"];
		        $this->load->library('image_lib', $config2);
		        if ( !$this->image_lib->resize())
		        {
		            // echo $this->image_lib->display_errors();exit;
		            $this->form_validation->set_rules('userfile1', $this->image_lib->display_errors());
		            $this->session->set_userdata(array('step2form_msg' => $this->image_lib->display_errors()));
		            //$this->load->view('template/regstpahead');
		            redirect('/registration/step2', 'refresh');
		        }
		      /* //  $data4 = $this->upload->data(); 
		        $config2['image_library'] = 'gd2';
		        $config2['source_image'] = './assets/profile/'.$this->upload->data('file_name');
		      //  $config2['create_thumb'] = TRUE;
		        $config2['maintain_ratio'] = FALSE;
		        $config2['quality'] = '60%';
		        $config2['width'] = 200;
		        $config2['height'] = 200;
		        $config2['new_image'] = './assets/profile/'.$this->upload->data('file_name');
		        $this->load->library('image_lib', $config2);
		        if ( !$this->image_lib->resize())
		        {
		           // echo $this->image_lib->display_errors();exit;
		            $this->form_validation->set_rules('userfile2', $this->image_lib->display_errors());
		            $this->session->set_userdata(array('step2form_msg' => $this->image_lib->display_errors()));
		            // $this->load->view('template/regstpahead');
		            redirect('/registration/step2', 'refresh');
		        } */
		        
		        $dataimage=array();
		        $dataimage['user_id']=$savedid;
		        $dataimage['name']=$this->upload->data('file_name');
		        $this->Business_logic->addToTable('user_profile_image',$dataimage);
		    }
		}
		
		
		$savedata=array();
	$savedata['user_id']=$savedid;
	$savedata['height']=$height;
	$savedata['martitalstatus']=$martitalstatus;
	$savedata['mother_tounge']=$mother_tounge;
	$savedata['show_profile']=$show_profile;
	$savedata['education']=$education;
	$savedata['occupation']=$occupation;
	$savedata['income']=$income;
	$savedata['eating']=$eating;
	$savedata['drinking']=$drinking;
	#$savedata['reg_step']=1;
	$savedata['manglik']=$manglik;
	$savedata['created']=time();
	$savedata['created_by']=$savedid;
	
	
	//$savedata['email']=$response1->email;
	$this->Business_logic->addToTable('user_personal_info',$savedata);
	$data1['reg_step']=2;
	$this->Business_logic->updateToTable('user_profile','user_id',$savedid,$data1);
	#echo "your data has been saved";
	redirect('/registration/step2#parentHorizontalTab2', 'refresh');
		//$this->load->view('template/regstpahead');
	#redirect('/dashboard', 'refresh');
		 
	}


	
	#registration/step2/partnerdetails
	
	public function registration_step2_partner_submit(){
		if(!$this->Business_logic->is_logged_in()){
			redirect('/home', 'refresh');
		}
		#	echo "jheerre";
		$age_from=$this->input->post('age_from');
		$age_to=$this->input->post('age_to');
		$height1=$this->input->post('height1');
		$height2=$this->input->post('height2');
		$martitalstatus_1=$this->input->post('martitalstatus_1');
		$religion1=$this->input->post('religion1');
		$mothertongue1=$this->input->post('mothertongue1');
		$p_status_1=$this->input->post('p_status_1');
		$education1=$this->input->post('education1');
		$occupation1=$this->input->post('occupation1');
		$income1=$this->input->post('income1');
		$star=$this->input->post('star');
		$manglik_1=$this->input->post('manglik_1');
		$eating_1=$this->input->post('eating_1');
		$drinking_1=$this->input->post('drinking_1');
		$savedid=  $this->session->userdata('user_id');;
	
		$savedata=array();
		$savedata['user_id']=$savedid;
		$savedata['height_from']=$height1;
		$savedata['height_to']=$height2;
		$savedata['age_from']=$age_from;
		$savedata['age_to']=$age_to;
		$savedata['martitalstatus']=$martitalstatus_1;
		$savedata['mother_tounge']=$mothertongue1;
		$savedata['p_status']=$p_status_1;
		$savedata['religion']=$religion1;
		$savedata['star']=$star;
		$savedata['education']=$education1;
		$savedata['occupation']=$occupation1;
		$savedata['income']=$income1;
		$savedata['eating']=$eating_1;
		$savedata['drinking']=$drinking_1;
		#$savedata['reg_step']=1;
		$savedata['manglik']=$manglik_1;
		$savedata['created']=time();
		$savedata['created_by']=$savedid;
	/* echo '<pre>';
	print_r($savedata);
	exit; */
		//$savedata['email']=$response1->email;
		$this->Business_logic->addToTable('user_partner_info',$savedata);
		$data1['reg_step']=3;
		$this->Business_logic->updateToTable('user_profile','user_id',$savedid,$data1);
		#echo "your data has been saved";
		//redirect('/registration/step2#parentHorizontalTab2', 'refresh');
		//$this->load->view('template/regstpahead');
		redirect('/dashboard', 'refresh');
			
	}
	
	
	
	
	

	public function dashboard(){
	   /*  echo "<pre>";
	    $data=$this->Business_logic->getRelatedUser("F",0,2);
	    print_r($data);exit; */
	    $query='';
	    $quick_search='';
	    if(isset($_REQUEST['submit']) && $_REQUEST['quick_search']==1){
	        $age_from=$this->input->post('age_from');
	        $age_to=$this->input->post('age_to');
	        $gender=$this->input->post('gender');
	        $religion=$this->input->post('religion');
	        $quick_search='&quick_search=1&g='.$gender.'&age_from='.$age_from.'&age_to='.$age_to.'&religion='.$religion;
	        if($_GET['quick_search']!=1){
	            redirect('/dashboard?'.$quick_search, 'refresh');
	        }
	    }elseif(!empty($_GET['q'])){
	        $query='&q='.$_GET['q'];
	    }
		if(!$this->Business_logic->is_logged_in()){
		    if(!empty($_GET['q'])){
		        $this->session->set_userdata(array('loginmessage' => 'Please Login First','search_data' => $query));
		        redirect('/home?'.$query, 'refresh');
		    }
		    if($_REQUEST['quick_search']==1){
		        $quick_search=$_SERVER['QUERY_STRING'];
		        $this->session->set_userdata(array('loginmessage' => 'Please Login First','quick_search' => $quick_search));
		        redirect('/home?'.$quick_search, 'refresh');
		    }
		    redirect('/home', 'refresh');
		}
	 //   print_r($_SESSION);
	    $username = $this->session->userdata('email');
	    $usertocken = $this->session->userdata('token');
	    $user_id = $this->session->userdata('user_id');
        if($this->session->userdata('search_data')){
            $query=$this->session->userdata('search_data');
            $this->session->unset_userdata('search_data');
            redirect('/dashboard?'.$query, 'refresh');
        }
        
        if($this->session->userdata('quick_search')){
            $session_quick_search=$this->session->userdata('quick_search');
            $this->session->unset_userdata('quick_search');
            redirect('/dashboard?'.$session_quick_search, 'refresh');
        }
        
        if($_GET['quick_search']==1){
            $quick_search=$_SERVER['QUERY_STRING'];
        }
        $page=$_GET['page'];
        $paginationapi="&page=".$page;
        $url=base_url().MYDASHBOARD.'?profileid='.$user_id.$query.$quick_search.$paginationapi;
	    $ch = curl_init($url);
	  // $data = json_encode($data_result);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	    curl_setopt($ch, CURLOPT_PORT, 80);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	        'X-Email:'.$username,
	        'X-Token:'.$usertocken,
	        'Content-Type:application/json',
	        
	    )
	    );
	    $response = curl_exec($ch);
	    curl_close($ch);
       //echo $response;
	    $response=json_decode($response,true);
	   // echo "<pre>";
	    //print_r($response);
	   // echo "</pre>";
	    $data=$response;
	    
	    $result['result']=$data['result'];
	    $result['result_awaiting']=$data['result'];
	    
	    $total_pages = ceil($data['count'] /NUM_OF_RECORD_DASHBOARD);
	    $page=$_GET['page'];
	    $prepage=0;
	    $nextpage=0;
	    if($page>1){
	        $prepage=$page-1;
	    }elseif($total_pages>$page){
	        $nextpage=$page+1;
	    }
	    $pagLink='<ul class="pagination">';
	    $pagLink.="<li class='page-item'><a class='page-link ' href='dashboard?&page=".$prepage.$query.$quick_search."'>Previous</a></li>";
	    for ($i=1; $i<=$total_pages; $i++) {
	        $pagLink .= "<li class='page-item'><a href='dashboard?&page=".$i.$query.$quick_search."'>".$i."</a></li>";
	    };  
	    $pagLink.="<li class='page-item'><a class='page-link' href='dashboard?&page=".$nextpage.$query.$quick_search."'>Next</a></li>";
	    $result['pagination']=$pagLink;
	    
	    $this->load->view('template/userhome',$result); # user landing home page
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}





