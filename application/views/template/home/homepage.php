<!DOCTYPE html>
<!-- html -->
<html>
<!-- head -->
<head>
<title>jat matrimonial services </title>
<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /><!-- bootstrap-CSS -->
<link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /><!-- Fontawesome-CSS -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type='text/javascript' src='<?php echo base_url()?>assets/js/jquery-2.2.3.min.js'></script>
<!-- Custom Theme files -->
<link href="<?php echo base_url()?>assets/css/menu.css" rel="stylesheet" type="text/css" media="all" /> <!-- menu style --> 
<!--theme-style-->
<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/easy-responsive-tabs.css " />
<!--meta data-->
<meta name="jatrishtey" content="web services for jat community">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="devendra singh">
<meta charset="utf-8">
<meta name="keywords" content="Jat matrimony, jat shaadi, jat rishtey, jat online shadi, jat marriage, jat rishtey in rajsthan, jat rishtey in up, jat rishtey in haryana, jat rishtey in punjab, jat rishtey delhi, jaat rishtey, jaats rishtey, jats matrimony, jats rishtey in bharat, jats matrimony in india" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--//meta data-->
<!-- online fonts -->
<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=devanagari,latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<!-- /online fonts -->
<!-- nav smooth scroll -->
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>	
<!-- //nav smooth scroll -->			
<!-- Calendar -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.css" />
	<script src="<?php echo base_url()?>assets/js/jquery-ui.js"></script>
		<script>
		  $(function() {
			$( "#datepicker" ).datepicker();
		 });
		</script>
<!-- //Calendar -->			
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/intlTelInput.css">
</head>
<!-- //head -->
<!-- body -->
<body>

<!-- header -->
<?php $this->load->view('template/header/header'); // added common footer?>
<!-- /header -->

<div class="w3layouts-banner" id="home">
<div class="container">
	<div class="logo">
		<h1><a class="cd-logo link link--takiri" href="index.html">Jatrishtey <span><i class="fa fa-heart" aria-hidden="true"></i>विश्वाश के रिश्ते.</span></a></h1>
	</div>
	<!--  <div class="clearfix"></div>--> 
	
	<div class="agileits-register">
		<h3>Register NOW!</h3>
		<?php if($this->session->userdata('message')){?>
		<div class="registration-msg" >
	
	   <?php echo $this->session->userdata('message');?>
	    </div>
	    <?php 
	    $this->session->unset_userdata('message');
       }?>
	
		<form action="/registration/step1" method="post" id="registrationstep1">
				<div class="w3_modal_body_grid">
					<span>Profile For:</span>
					<select id="w3_country" name="profile_for"  class="frm-field required profile_for">
											
					</select>
				</div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>First Name:</span>
					<input type="text" name="fname" placeholder=" " required="required"  id="fname" />
					<div id="fname_error" class="help_text" style="display:none;"></div>
				</div>
				<div class="w3_modal_body_grid">
					<span>Last Name:</span>
					<input type="text" name="lname" placeholder=" " required="required" on id="lname" />
					<div id="lname_error" class="help_text" style="display:none;">fff</div>
				</div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>Gender:</span>
					<div class="w3_gender">
						<div class="colr ert">
							<label class="radio"><input type="radio" name="gender" value="M" required="required"><i></i>Male</label>
						</div>
						<div class="colr">
							<label class="radio"><input type="radio" name="gender" value="F" required="required"><i></i>Female</label>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>Date Of Birth:</span>
					<?php echo form_input( array( 'name' => 'dob', 'type' => 'date', 'value' => 'dd-mm-yyyy' , 'class' => 'date' , 'required' => 'true' ) );?>
				</div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>religion:</span>
					<select id="w3_country1" name="religion_id"  class="frm-field required religion_list" required="required">  
					</select>
				</div>
				<div class="clearfix"> </div>
				<div class="w3_modal_body_grid">
					<span>Gotra:</span>
					<select id="w3_country1" name="gotra_id"  class="frm-field required gotra_list" required="required"> 
						  						
					</select>
				</div>
			<div class="clearfix"> </div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>Mother Gotra:</span>
					<select id="w3_country1" name="mother_gotra" class="frm-field required mother_gotra_list" required="required"> 
						 						
					</select>
				</div>
			<div class="clearfix"> </div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
				<span>Mobile No:</span>
				<!-- country codes (ISO 3166) and Dial codes. -->
					<input id="phone" type="tel" name="mobile" required="required">
				  <!-- Load jQuery from CDN so can run demo immediately -->
				  <script src="<?php echo base_url()?>assets/js/intlTelInput.js"></script>
				  <script>
					$("#phone").intlTelInput({
					   allowDropdown: false,
					 
					  onlyCountries: ['in'],
					 
					  utilsScript: "<?php echo base_url()?>assets/js/utils.js"
					});
				  </script>
				</div>
				<div class="w3_modal_body_grid">
					<span>Email:</span>
					<input type="email" name="email" placeholder=" " required="required"/>
				</div>
				<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>Password:</span>
					<input type="password" name="password" placeholder=" " required="required"/>
				</div>
				
				<div class="clearfix"> </div>
			<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>Countries</span>
					<select id="w3_country1"  name="country_id" class="frm-field required country_list" required="required"> 
						   						
					</select>
				</div>
				<div class="w3_modal_body_grid">
					<span>State</span>
					<select id="w3_country1" name="state_id" class="frm-field required state_list" required="required"> 
											
					</select>
				</div>
			<div class="clearfix"> </div>
			<div class="w3_modal_body_grid w3_modal_body_grid1">
					<span>City</span>
					<select id="w3_country1" name="district_id" class="frm-field required city_list" required="required"> 
						  						
					</select>
				</div>
			<div class="clearfix"> </div>
				<div class="w3-agree">
					<input type="checkbox" id="c1" name="cc" required="required">
					<label class="agileits-agree">I have read & agree to the <a href="terms.html">Terms and Conditions</a></label>
				</div>
				<input type="submit" value="Register me" name="registration"  />
				<div class="clearfix"></div>
				<p class="w3ls-login">Already a member? <a href="#" data-toggle="modal" data-target="#myModal">Login</a></p>
			</form>
		</div>
		<?php if($this->session->userdata('loginmessage')){?>
		<script>
                 $(function(){
                     $('#myModal').modal('show');
                 });
        </script>
        <?php  }?>
		<!-- Modal -->
				<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
						<h4 class="modal-title">Login to Continue</h4>
					  </div>
					  <div class="modal-body">
					  <?php if($this->session->userdata('loginmessage')){?>
                    	<div class="registration-msg"><?php echo $this->session->userdata('loginmessage');?></div>
                    	    <?php $this->session->unset_userdata('loginmessage');
                      }?>
						<div class="login-w3ls">
							<form id="" action="/login_user" method="post">
								<label>Email id </label>
								<input type="text" name="email" placeholder="Email id" required="">
								<label>Password</label>
								<input type="password" name="password" placeholder="Password" required="">	
								<div class="w3ls-loginr"> 
									<input type="checkbox" id="brand" name="checkbox" value="">
									<span> Remember me ?</span> 
									<a href="#">Forgot password ?</a>
								</div>
								<div class="clearfix"> </div>
								<input type="submit" name="submit" value="Login">
								<div class="clearfix"> </div>
								<div class="social-icons">
									<ul>  
										<li><a href="#"><span class="icons"><i class="fa fa-facebook" aria-hidden="true"></i></span><span class="text">Facebook</span></a></li> 
										<li class="twt"><a href="#"><span class="icons"><i class="fa fa-twitter" aria-hidden="true"></i></span><span class="text">Twitter</span></a></li>  
									</ul> 
									<div class="clearfix"> </div>
								</div>	
							</form>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				<!-- //Modal -->
	</div>
</div>
<!-- Find your soulmate -->
<!-- 
	<div class="w3l_find-soulmate text-center">
		<h3>Find Your Soulmate</h3>
			<div class="container">
				<a class="scroll" href="#home">
					<div class="col-md-3 w3_soulgrid">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
						<h3>Sign Up</h3>
						<p>Signup for free and Upload your profile</p>
					</div>
				</a>
				<a class="scroll" href="#home">
					<div class="col-md-3 w3_soulgrid">
						<i class="fa fa-search" aria-hidden="true"></i>
						<h3>Search</h3>
						<p>Search for your right partner</p>
					</div>
				</a>
				<a class="scroll" href="#home">
					<div class="col-md-3 w3_soulgrid">
						<i class="fa fa-users" aria-hidden="true"></i>
						<h3>Connect</h3>
						<p>Connect your perfect Match</p>
					</div>
				</a>
				<a class="scroll" href="#home">
					<div class="col-md-3 w3_soulgrid">
						<i class="fa fa-comments-o" aria-hidden="true"></i>
						<h3>Interact</h3>
						<p>Become a member and start Conversation</p>
					</div>
				</a>
				<div class="clearfix"> </div>
			</div>
	</div>
	-->
	<!-- //Find your soulmate -->
		
		<!-- featured profiles -->			
		
		<div class="w3l_find-soulmate text-center">
			<div class="container">&nbsp;&nbsp;&nbsp;
			</div>
		</div>
		 <div class="w3layouts_featured-profiles">
				<div class="container">
				<!-- slider -->
				<div class="agile_featured-profiles">
					<h2>Featured Profiles</h2>
							<ul id="flexiselDemo3">
								<li>
											<?php 
					$data=$this->Business_logic->getUserDetails("M",0,8);#business logic data
                      foreach ($data as $res){
                      	$from = new DateTime(date('Y-m-d',$res->dob));
                      	$to   = new DateTime('today');
                      	#echo
                      		$datacond['user_id']=$res->user_id;
                     #	print_r($datacond);
                      #	exit;
                      	$profile_value=$this->Business_logic->getSingleTableData('user_personal_info',$datacond,'0',1,'id','desc');
                      
                      	#exit;
                      	$datavale['id']=$res->religion_id;
                      	$religion=$this->Business_logic->getSingleTableData('religion',$datavale);
                      		
                      	$datavale['id']=$res->gotra_id;
                      	$gotra=$this->Business_logic->getSingleTableData('gotra',$datavale);
                      		
                      	$datavale['id']=$profile_value['0']['education'];
                      	$education=$this->Business_logic->getSingleTableData('education',$datavale);
                      		
                      	$datavale['id']=$profile_value['0']['occupation'];
                      	$occupation=$this->Business_logic->getSingleTableData('occupation',$datavale);
                      		
                      	$datavale['id']=$profile_value['0']['height'];
                      	$height=$this->Business_logic->getSingleTableData('heightgroombride',$datavale);
                      		
                      	$datavale1['state_id']=$res->state_id;
                      	$states=$this->Business_logic->getSingleTableData('states',$datavale1);
                      	
                      	$datavale2['district_id']=$res->district_id;
                      	$districts=$this->Business_logic->getSingleTableData('districts',$datavale2);
                      	$profileimg['user_id']=$res->user_id;
                      	$img=$this->Business_logic->getSingleTableData('user_profile_image',$profileimg,0,1,'id','desc');
                      	$path="images/profile-image-men.jpg";
                      	if(!empty($img['0']['name'])){
                      		$path="profile/".$img['0']['name'];
                      	}
                      				?>
									<div class="col-md-3 biseller-column">
										<a href="/view/profile/<?php echo $res->user_id;?>">
											<div class="profile-image">
												<img src="<?php //echo base_url()?>assets/<?php echo $path?>" class="img-responsive" alt="profile image">
												<div class="agile-overlay">
												<h4>Profile ID: <?php echo "JATM".$res->user_id?></h4>
													<ul>
														<li><span>Age / Height</span>: <?php echo $height['0']['name']?></li>
														<li><span>Caste</span>: <?php echo $gotra[0]['name']?></li>
														<li><span>Religion</span>: <?php echo $religion[0]['name']?></li>
														<li><span>Profession</span>: <?php echo $occupation['0']['name']?></li>
														<li><span>Education</span>: <?php echo $education['0']['name']?></li>
														<li><span>City</span>: <?php echo $districts['0']['name']?></li>
													</ul>
												</div>
											</div>
										</a>
									</div>
									<?php }?>
								</li>
								<li>
									<?php $data=$this->Business_logic->getUserDetails("F",0,8);#business logic data
                     
                      
                      foreach ($data as $res){
                      	$from = new DateTime(date('Y-m-d',$res->dob));
                      	$to   = new DateTime('today');
                      	#echo
                      	$datacond['user_id']=$res->user_id;
                     #	print_r($datacond);
                      #	exit;
                      	$profile_value=$this->Business_logic->getSingleTableData('user_personal_info',$datacond,'0',1,'id','desc');
                      
                      	#exit;
                      	$datavale['id']=$res->religion_id;
                      	$religion=$this->Business_logic->getSingleTableData('religion',$datavale);
                      		
                      	$datavale['id']=$res->gotra_id;
                      	$gotra=$this->Business_logic->getSingleTableData('gotra',$datavale);
                      		
                      	$datavale['id']=$profile_value['0']['education'];
                      	$education=$this->Business_logic->getSingleTableData('education',$datavale);
                      		
                      	$datavale['id']=$profile_value['0']['occupation'];
                      	$occupation=$this->Business_logic->getSingleTableData('occupation',$datavale);
                      		
                      	$datavale['id']=$profile_value['0']['height'];
                      	$height=$this->Business_logic->getSingleTableData('heightgroombride',$datavale);
                      		
                      	$datavale1['state_id']=$res->state_id;
                      	$states=$this->Business_logic->getSingleTableData('states',$datavale1);
                      	
                      	$datavale2['district_id']=$res->district_id;
                      	$districts=$this->Business_logic->getSingleTableData('districts',$datavale2);
                      	
                      	$profileimg['user_id']=$res->user_id;
                      	$img=$this->Business_logic->getSingleTableData('user_profile_image',$profileimg,0,1,'id','desc');
                      	$path="images/profile-image-girl.jpg";
                      	if(!empty($img['0']['name'])){
						$path="profile/".$img['0']['name'];
							}
                      				?>
									<div class="col-md-3 biseller-column">
										<a href="/view/profile/<?php echo $res->user_id;?>">
											<div class="profile-image">
												<img src="<?php //echo base_url()?>assets/<?php echo $path?>" class="img-responsive" alt="profile image">
												<div class="agile-overlay">
												<h4>Profile ID: <?php echo "JATM".$res->user_id?></h4>
													<ul>
														<li><span>Age / Height</span>: <?php echo $height['0']['name']?></li>
														<li><span>Caste</span>: <?php echo $gotra[0]['name']?></li>
														<li><span>Religion</span>: <?php echo $religion[0]['name']?></li>
														<li><span>Profession</span>: <?php echo $occupation['0']['name']?></li>
														<li><span>Education</span>: <?php echo $education['0']['name']?></li>
														<li><span>City</span>: <?php echo $districts['0']['name']?></li>
													</ul>
												</div>
											</div>
										</a>
									</div>
									<?php }?>
								</li>
							</ul>
					</div>   
			</div>
			<!-- //slider -->
			</div> 
			<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.flexisel.js"></script><!-- flexisel-js -->	
					<script type="text/javascript">
						 $(window).load(function() {
							$("#flexiselDemo3").flexisel({
								visibleItems:1,
								animationSpeed: 1000,
								autoPlay: true,
								autoPlaySpeed: 4000,    		
								pauseOnHover: true,
								enableResponsiveBreakpoints: true,
								responsiveBreakpoints: { 
									portrait: { 
										changePoint:480,
										visibleItems:1
									}, 
									landscape: { 
										changePoint:640,
										visibleItems:1
									},
									tablet: { 
										changePoint:768,
										visibleItems:1
									}
								}
							});
							
						});
					   </script>
			<!-- //featured profiles -->		   
					   
	<!-- mobile-app -->
	<?php //$this->load->view('template/footer/topfooter'); // added common footer?>
	<!-- /mobile-app -->
	
	<!-- browse profiles -->
	<?php $this->load->view('template/footer/footer1'); // added common footer?>
	<!-- //browse profiles -->
	
	<!-- Assisted Service -->
	<?php $this->load->view('template/footer/footer01'); // added common footer?>
	<!-- //Assisted Service -->
	
	<!-- Location -->
	<?php $this->load->view('template/footer/footer0'); // added common footer?>
	<!-- //Location -->
	
	<!-- Get started -->
	<div class="w3layous-story text-center">
		<div class="container">
					
		<!-- Start of SimpleHitCounter Code -->
		
			<!-- End of SimpleHitCounter Code -->
			<h4>
			Users visit- <img src="http://simplehitcounter.com/hit.php?uid=2325500&f=16711680&b=16777215" border="0" height="25" width="100" alt="web counter">
			, Your story is waiting to happen!  <a class="scroll" href="#home">Get started</a></h4>
		</div>
	</div>
	<!-- //Get started -->
	
<!-- footer -->
<?php $this->load->view('template/footer/footer'); // added common footer?>
<!-- //footer -->	
<!-- menu js aim -->
	<script src="<?php echo base_url()?>assets/js/jquery.menu-aim.js"> </script>
	<script src="<?php echo base_url()?>assets/js/main.js"></script> <!-- Resource jQuery -->
	<!-- //menu js aim -->
	<!-- for bootstrap working -->
		<script src="<?php echo base_url()?>assets/js/bootstrap.js"></script>
		<script src="<?php echo base_url()?>assets/js/helptext.js"></script> <!-- Resource jQuery -->
<!-- //for bootstrap working -->
		 

	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
	  			containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
	 		};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
							
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!-- for smooth scrolling -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/move-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/easing.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
	</script>
	
	
	
	<script src="<?php echo base_url()?>assets/js/api.js"></script> <!-- Resource jQuery -->
	
	
	
	
<!-- //for smooth scrolling -->
	
	<!-- //for smooth scrolling -->
</body>
<!-- //body -->
</html>
<!-- //html -->
