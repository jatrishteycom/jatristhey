<header>
	<!--  Navigation Start -->
 <div class="navbar navbar-inverse-blue navbar">
    <!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
      <div class="navbar-inner">
        <div class="container">
          <div class="menu">
					<div class="cd-dropdown-wrapper">
						<a class="cd-dropdown-trigger" href="#0">Browse Profiles by</a>
						<nav class="cd-dropdown"> 
							<a href="#0" class="cd-close">Close</a>
							<ul class="cd-dropdown-content"> 
								<li><a href="matches.html">All Profiles</a></li>
								<li class="has-children">
									<a href="#">Height</a> 
									<?php $heigtdata=$this->Business_logic->getDistinctProfile('user_personal_info','height');
									$gotra_data=$this->Business_logic->getDistinctProfile('user_profile','gotra_id');
									$religion_data=$this->Business_logic->getDistinctProfile('user_profile','religion_id');
									$city_data=$this->Business_logic->getDistinctProfile('user_profile','district_id');
									$state_data=$this->Business_logic->getDistinctProfile('user_profile','state_id');
									$occupation_data=$this->Business_logic->getDistinctProfile('user_personal_info','occupation');
									?>
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
										<?php foreach ($heigtdata as $res){

											$datavale['id']=$res->height;
											$height=$this->Business_logic->getSingleTableData('heightgroombride',$datavale);
											?>
												<li><a href="/dashboard?&q=height=<?php echo $res->height?>"><?php echo $height['0']['name']?></a></li>
												<?php }?>
												
									</ul> <!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->
								<li class="has-children">
									<a href="#">Gotra</a> 
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<?php foreach ($gotra_data as $res){
										$datavale['id']=$res->gotra_id;
                      				$gotra=$this->Business_logic->getSingleTableData('gotra',$datavale);
											?>
												<li><a href="/dashboard?&q=gotra_id=<?php echo $res->gotra_id?>"><?php echo $gotra['0']['name']?></a></li>
												<?php }?>
									</ul> <!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->
								<li class="has-children">
									<a href="products2.html">Religion</a> 
									<ul class="cd-secondary-dropdown is-hidden"> 
										<li class="go-back"><a href="#">Menu</a></li>
											<?php foreach ($religion_data as $res){
										$datavale['id']=$res->religion_id;
                      					$religion=$this->Business_logic->getSingleTableData('religion',$datavale);
											?>
												<li><a href="/dashboard?&q=religion_id=<?php echo $res->religion_id?>"><?php echo $religion['0']['name']?></a></li>
												<?php }?>
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children --> 
								<li class="has-children">
									<a href="#">City</a> 
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<?php foreach ($city_data as $res){
										$datavale2['district_id']=$res->district_id;
                      	$districts=$this->Business_logic->getSingleTableData('districts',$datavale2);
											?>
												<li><a href="/dashboard?&q=district_id=<?php echo $res->district_id?>"><?php echo $districts['0']['name']?></a></li>
												<?php }?>
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->  
								<li class="has-children">
									<a href="#">Occupation</a>
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
            										<?php 
            										$data=array();
            										foreach ($occupation_data as $res){
            										    $data['id']=$res->occupation;
            										    $occupation=$this->Business_logic->getSingleTableData('occupation',$data);
            											?>
												<li><a href="/dashboard?&q=occupation=<?php echo $res->occupation?>"><?php echo $occupation['0']['name']?></a></li>
												<?php }?>
												
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->  
								<li class="has-children">
									<a href="#">State</a>
									<ul class="cd-secondary-dropdown is-hidden">
										<li class="go-back"><a href="#">Menu</a></li>
												<?php foreach ($state_data as $res){
											$datavale1['state_id']=$res->state_id;
                      	$states=$this->Business_logic->getSingleTableData('states',$datavale1);
											?>
												<li><a href="/dashboard?&q=state_id=<?php echo $res->state_id?>"><?php echo $states['0']['State']?></a></li>
												<?php }?>
									</ul><!-- .cd-secondary-dropdown --> 
								</li> <!-- .has-children -->  
								 
							</ul> <!-- .cd-dropdown-content -->
						</nav> <!-- .cd-dropdown -->
					</div> <!-- .cd-dropdown-wrapper -->	 
				</div>
           <div class="pull-right">
          	<nav class="navbar nav_bottom" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header nav_2">
		      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">Menu
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		   </div> 
		   <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
		        <ul class="nav navbar-nav nav_1">
		            <li class="active"><a href="<?php echo base_url();?>">Home</a></li>
		            <li><a href="<?php echo base_url();?>feedback/about">About</a></li>
		            <!--  <li><a href="search.html">Search</a></li>  -->
		            <li><a href="<?php echo base_url();?>feedback/feed">Feedback</a></li>
					  <!--<li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Search<span class="caret"></span></a>
		              <ul class="dropdown-menu" role="menu">
		                <li><a href="search.html">Regular Search</a></li>
		                <li><a href="profile.html">Recently Viewed Profiles</a></li>
		                <li><a href="search-id.html">Search By Profile ID</a></li>
		                <li><a href="faq.html">Faq</a></li>
		                <li><a href="shortcodes.html">Shortcodes</a></li>
		              </ul>
		            </li>-->
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Quick Search<span class="caret"></span></a>
		              <ul class="dropdown-menu" role="menu">
		                <div class="banner-bottom-login">
		<div class="w3agile_banner_btom_login">
		<?php 
		$gotra_data=$this->Business_logic->getDistinctProfile('user_profile','gotra_id');
		
		
		?>
			<form action="/dashboard" method="post">
				<div class="w3agile__text w3agile_banner_btom_login_left">
					<h4>I'm looking for a</h4>
					<select id="country1" onchange="change_country(this.value)" name="gender" class="frm-field required">
						<option value="M">Male</option>
						<option value="F">Female</option>   
					</select>
				</div>
				<div class="w3agile__text w3agile_banner_btom_login_left1">
					<h4>Aged</h4>
					<select id="country1" onchange="change_country(this.value)" name="age_from" class="frm-field required">
						<option value="20">20</option>
						<option value="21">21</option>   
						<option value="22">22</option>   
						<option value="23">23</option>   
						<option value="24">24</option>   
						<option value="25">25</option>  
						<option value="0">- - -</option>   					
					</select>
					<span>To </span>
					<select id="country2" onchange="change_country(this.value)" name="age_to" class="frm-field required">
						<option value="30">30</option>
						<option value="31">31</option>   
						<option value="32">32</option>   
						<option value="33">33</option>   
						<option value="34">34</option>   
						<option value="35">35</option>  
						<option value="0">- - -</option>   					
					</select>
				</div>
				<div class="w3agile__text w3agile_banner_btom_login_left2">
					<h4>Religion</h4>
					<select id="country3" onchange="change_country(this.value)" name="religion" class="frm-field required">
						<option value="2">Hindu</option>  
						<option value="1">Muslim</option>   
						<option value="3">Sikh</option>   
						<option value="0">No Religious Belief</option>   					
					</select>
				</div>
				<div class="w3agile_banner_btom_login_left3">
					<input type="hidden" value="1" name="quick_search" />
					<input type="submit" value="Search" name="submit"/>
				</div>
				<div class="clearfix"> </div>
			</form>
		</div>
	</div>
		              </ul>
		            </li>
		            <li ><a href="<?php echo base_url();?>feedback/contact">Contact</a></li>
		            <?php 
		            if(!$this->Business_logic->is_logged_in()){
		               echo '<li class="last"><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>';
		            }else{
		               echo  '<li class="last"><a href="/logout">Logout</a></li>';
		            }
		            ?>
		        </ul>
		     </div><!-- /.navbar-collapse -->
		    </nav>
		   </div> <!-- end pull-right -->
          <div class="clearfix"> </div>
        </div> <!-- end container -->
      </div> <!-- end navbar-inner -->
    </div> <!-- end navbar-inverse-blue -->
<!-- ============================  Navigation End ============================ -->
</header>