<div class="wthree-mobilaapp main-grid-border">
	<div class="container">
		<div class="app">
			<div class="col-md-6 w3ls_app-left mpl">
				<h3>Matrimonial mobile app on your smartphone!</h3>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				<div class="agileits_app-devices">
					<h5>Download the App</h5>
					<a href="#"><img src="<?php echo base_url()?>assets/images/1.png" alt=""></a>
					<a href="#"><img src="<?php echo base_url()?>assets/images/2.png" alt=""></a>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-offset-1 col-md-5 app-image">
				<img src="<?php echo base_url()?>assets/images/mob.png" alt="">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>