<?php 
$gotra_data=$this->Business_logic->getDistinctProfile('user_profile','gotra_id');
$city_data=$this->Business_logic->getDistinctProfile('user_profile','district_id');
$state_data=$this->Business_logic->getDistinctProfile('user_profile','state_id');
?>


	<!-- browse profiles -->
	<div class="w3layouts-browse text-center">
		<div class="container">
			<h3>Browse Matchmaking Profiles by</h3>
			<div class="col-md-4 w3-browse-grid">
				<h4>By State</h4>
				<ul>
				
				<?php foreach ($state_data as $res){
					$datavale8['state_id']=$res->state_id;
                     $states=$this->Business_logic->getSingleTableData('states',$datavale8);
					?>
					<li><a href="/dashboard?&q=state_id=<?php echo $res->state_id?>"><?php echo $states['0']['State']?></a></li>
			<?php }?>
					
					<li class="more"><a href="#">more..</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3-browse-grid">
				<h4>By City</h4>
				<ul>
				<?php foreach ($city_data as $res){
				    $datavale9['district_id']=$res->district_id;
                    $districts=$this->Business_logic->getSingleTableData('districts',$datavale9);
					?>
					<li><a href="/dashboard?&q=district_id=<?php echo $res->district_id?>"><?php echo $districts['0']['name']?></a></li>
			<?php }?>
					<li class="more"><a href="#">more..</a></li>
				</ul>
			</div>
			<div class="col-md-4 w3-browse-grid">
				<h4>By NRI/Country</h4>
				<ul>
				<?php foreach ($gotra_data as $res){
	               $datavale10['id']=$res->gotra_id;
                    $gotra=$this->Business_logic->getSingleTableData('gotra',$datavale10);
	               	?>
					<li><a href="/dashboard?&q=gotra_id=<?php echo $res->gotra_id?>"><?php echo $gotra['0']['name']?></a></li>
			<?php }?>
					
					<li class="more"><a href="#">more..</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- //browse profiles -->
	