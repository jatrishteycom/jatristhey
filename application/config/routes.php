<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route[HOME_PAGE_URL] = 'web/home';
$route[REGISTRATION_STEP1_URL] = 'web/home/registration_step1';
$route[LOGIN_PAGE] = 'web/home/login_user';
$route[LOGOUT] = 'web/home/logout';
$route[LOGIN_WEB] = 'web/Login/login';
$route[REGISTRATION_STEP2_URL] = 'web/home/registration_step2';
$route[REGISTRATION_STEP2_SUBMIT_URL] = 'web/home/registration_step2_submit';
$route[REGISTRATION_STEP2_PROFILE_SUBMIT_URL] = 'web/home/registration_step2_partner_submit';
$route[DASHBOARD] = 'web/home/dashboard';
$route[USERPROFILEDETAIL.'/:num'] = 'web/Dashboard/Myprofile/userprofile';
$route[LOGIN_USER] = 'web/Login/Login';
$route[LOGIN_USER_SUBMIT] = 'web/Login/Login/loginsubmit';

/**
Api URL
 */
$route[GETPROFILELIST] = 'Rest/Api/getProfileFor';// get change request impact list
$route[GETCOUNTRYLIST] = 'Rest/Api/getCountries';// List of Countries
$route[GETRELIGION] = 'Rest/Api/getReligion';// List of Countries
$route[GETGOTRA] = 'Rest/Api/getGotra';// List of Countries
$route[GETSTATE] = 'Rest/Api/getState';// List of State
$route[GETCITY] = 'Rest/Api/getCity';// List of City
$route[GETDISTRICT] = 'Rest/Api/getDsitrict';// List of City
$route[ADD_STEP1] = 'Rest/Api/Registratiostep1Submit';// Submit 1
$route[LOGIN] = 'Rest/Api/login';// Submit 1
$route[MYDASHBOARD] = 'Rest/Api/getRelatedProfile';// Submit 1
$route[DASHBOARDAWAITEDPROFILE] = 'Rest/Api/getRelatedProfile';// Submit 1
$route[LIKE_PROFILE] = 'Rest/Api/LikeProfile';// Submit 1
$route[SEND_MESSAGE] = 'Rest/Api/SendMessage';// Submit 1
#$route['default_controller'] = 'home';
#$route['default_controller'] = 'web/home';
$route['default_controller'] = "Welcome";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
