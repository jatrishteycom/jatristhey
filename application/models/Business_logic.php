<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Business_logic extends CI_Model {
	/**
	 * add single entry in table
	 $tablename=table name
	 $post=data in array formats like array('id'=>$val['id'],'name'=>$name)
	 */
	public function addToTable($tablename,$post){
		$CI = &get_instance();
		$this->db = $CI->load->database('db', TRUE);
		$insert = $this->db->insert($tablename, $post);
		$insert_id = $this->db->insert_id();
		if (!$insert_id){
			return false;
		}else{return $insert_id;
		}
	}
	
	/**
	 * Data for single Table
	 */
	
	public function getSingleTableData($tablename,$condition=[],$start='',$limit='',$orderid='',$ordertype='asc'){
		$CI = &get_instance();
		$this->db3 = $CI->load->database('db', TRUE);
		$this->db3->select('*');
		$this->db3->from($tablename);
		foreach ($condition as $key => $value){ //infinite number of conditions
			$this->db3->where($key, $value);
		}
		if($limit!='' && $start!=''){
			$this->db3->limit($limit, $start);
		}
		if(!empty($orderid))
			$this->db3->order_by("$orderid", "$ordertype");
		$query = $this->db3->get();
		$result=$query->result_array();
		return $result;
	}
	
	
	/**
	 * fetch Asset List Using DC Id and Assets Id
	 */
	public function getRelatedUser($gender='F',$start=0,$limit=8,$orderid='',$ordertype='asc',$gendertype=TRUE){

		$CI = &get_instance();
		$this->db3 = $CI->load->database('db', TRUE);
		$this->db3->select('*');
		$this->db3->from('users');
		$this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
		$this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
		//	$this->db3->group_by('KS_DI.DC_ID');
		if($gendertype){
		$this->db3->where("user_profile.gender", $gender);
		}
		else{
			$this->db3->where("users.user_id", $orderid);
		}
		$this->db3->where("users.status !=", 'D' );
		if($limit>0){
			$this->db3->limit($limit, $start);
		}
		#$this->db3->group_by('user_personal_info.user_id');
		$query = $this->db3->get();
		$result['data']= $query->result();
		
		$CI = &get_instance();
		$this->db3 = $CI->load->database('db', TRUE);
		$this->db3->select('*');
		$this->db3->from('users');
		$this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
		$this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
		if($gendertype){
		    $this->db3->where("user_profile.gender", $gender);
		}
		else{
		    $this->db3->where("users.user_id", $orderid);
		}
		$this->db3->where("users.status !=", 'D' );
		$result['count']= $this->db3->count_all_results();
		
		return $result;
	}
	
	
	/**
	 * fetch Asset List Using DC Id and Assets Id
	 */
	public function getUserDetails($gender='F',$start=0,$limit=10,$orderid='',$ordertype='asc',$gendertype=TRUE){
		$CI = &get_instance();
		$this->db3 = $CI->load->database('db', TRUE);
		$this->db3->select('*');
		$this->db3->from('users');
		$this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
		#$this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
		//	$this->db3->group_by('KS_DI.DC_ID');
		if($gendertype){
		$this->db3->where("user_profile.gender", $gender);
		}
		else{
			$this->db3->where("users.user_id", $orderid);
		}
		$this->db3->where("users.status !=", 'D' );
		$this->db3->order_by("users.user_id","desc");
	//	if($limit!='' && $start!=''){
			$this->db3->limit($limit, $start);
	//	}
		
		#$this->db3->group_by('user_personal_info.user_id');
		$query = $this->db3->get();
		/* echo "=============>???";
		print_r($this->db3->last_query());
		exit; */
		$result= $query->result();
		return $result;
		}
	/**
	 * Set Session after login
	 * @param unknown $tablename
	 * @param unknown $post
	 * @return boolean|unknown
	 */
	public function setSession($data){
	    if($data){
	        $this->session->set_userdata($data);
	        return true;
	    }else{
	        return false;
	    }
	}
	
	public function is_logged_in(){
	    $username = $this->session->userdata('email');
	    $usertocken = $this->session->userdata('token');
	    $user_expirytime = $this->session->userdata('experi_time');
	    if(isset($username) && isset($usertocken) && isset($user_expirytime)){
	        $currenttime=time();
	        if($user_expirytime>$currenttime){
	            return true;
	        }else{
	            return false;
	        }
	    }else{
	        return false;
	    }
	}
	
	
	

	/**
	 * Update Table
	 * @param unknown $tablename
	 * @param unknown $where
	 * @param unknown $id
	 * @param unknown $post
	 * @return boolean
	 */
	
	public function updateToTable($tablename,$where,$id,$post,$type='db'){
		$CI = &get_instance();
		$this->$type = $CI->load->database($type, TRUE);
		$this->$type->where($where, $id);
		$update = $this->$type->update($tablename, $post);
		if (!$update)
		{
			return false;
		}
		else{
			return true;
		}
	}
	
	
	/**
	 * Update Table with muti condition
	 * @param unknown $tablename
	 * @param unknown $where
	 * @param unknown $id
	 * @param unknown $post
	 * @return boolean
	 */
	
	public function updateTablewithMulticond($tablename,$where=[],$post,$type='db'){
		$CI = &get_instance();
		$this->$type = $CI->load->database($type, TRUE);
		foreach ($where as $res=>$val){
		$this->$type->where($res, $val);
		}
		$update = $this->$type->update($tablename, $post);
		if (!$update)
		{
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
		User step 
	 */
	function getUserRegstep($reg_step=''){
		if(empty($reg_step)){
		$data['user_id']=$_SESSION['user_id'];
		$userdata=$this->Business_logic->getSingleTableData('user_profile',$data);#business logic
		$reg_step=$userdata['0']['reg_step'];
		}
		if($reg_step==1){
			redirect('/registration/step2', 'refresh');
		}
		else if($reg_step==2){
			redirect('/registration/step2#parentHorizontalTab2', 'refresh');
		}
		else if($reg_step==3){
			redirect('/dashboard', 'refresh');
		}
		
	}
	
	/**
	 User step
	 */
	function getDistinctProfile($table,$id,$cond=''){
		$CI = &get_instance();
		$this->db3 = $CI->load->database('db', TRUE);
		$this->db3->select($id);
		$this->db3->from($table);
		$this->db3->group_by($id);
		#$this->db3->where("users.status !=", 'D' );
		
		$query = $this->db3->get();
		$result= $query->result();
		return $result;
	
	}
	

	
	/**
	 * fetch data by searching
	 */
	public function getUserSearchList($key,$value,$start=0,$limit=10,$orderid='',$ordertype='asc'){
	    $CI = &get_instance();
	    $this->db3 = $CI->load->database('db', TRUE);
	    $this->db3->select('*');
	    $this->db3->from('users');
	    $this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
	    $this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
	    //	$this->db3->group_by('KS_DI.DC_ID');
	    if($key=='height' || $key=='occupation' || $key=='height'){
	        $this->db3->where("user_profile.".$key, $value);
	    }else{
	        $this->db3->where("user_profile.".$key, $value);
	    }
	    $this->db3->where("users.status !=", 'D' );
	    if($limit!='' && $start!=''){
	        $this->db3->limit($limit, $start);
	    }
	    $query = $this->db3->get();
	    $result['data']= $query->result();
	    /* 	print_r($this->db3->last_query());
	     exit; */
	    
	    
	    $CI = &get_instance();
	    $this->db3 = $CI->load->database('db', TRUE);
	    $this->db3->select('*');
	    $this->db3->from('users');
	    $this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
	    $this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
	    //	$this->db3->group_by('KS_DI.DC_ID');
	    if($key=='height' || $key=='occupation' || $key=='height'){
	        $this->db3->where("user_profile.".$key, $value);
	    }else{
	        $this->db3->where("user_profile.".$key, $value);
	    }
	    $this->db3->where("users.status !=", 'D' );
	    $result['count']= $this->db3->count_all_results();
	    
	    return $result;
	}
	
	
	/**
	 * fetch data by searching
	 */
	public function getUserQuickSearchList ($gender,$age_from=0,$age_to=0,$religion=0, $start=0,$limit=10){
	    $CI = &get_instance();
	    $this->db3 = $CI->load->database('db', TRUE);
	    $this->db3->select('*');
	    $this->db3->from('users');
	    $this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
	    $this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
	    $this->db3->where("user_profile.gender", "$gender");
	    if(!empty($age_from)){
	        $this->db3->where("TIMESTAMPDIFF(YEAR, FROM_UNIXTIME(user_profile.dob), NOW()) >=",$age_from);
	    }
	    if(!empty($age_to)){
	        $this->db3->where("TIMESTAMPDIFF(YEAR, FROM_UNIXTIME(user_profile.dob), NOW()) <=",$age_to);
	    }
	    if(!empty($religion)){
	        $this->db3->where("user_profile.religion_id", $religion);
	    }
	    $this->db3->where("users.status !=", 'D' );
	    if($limit>$limit){
	    	$this->db3->limit($limit, $start);
	    }
	    $query = $this->db3->get();
	    $result['data']= $query->result();
	    
	    
	    
	    $CI = &get_instance();
	    $this->db3 = $CI->load->database('db', TRUE);
	    $this->db3->select('*');
	    $this->db3->from('users');
	    $this->db3->join('user_profile', 'users.user_id = user_profile.user_id', 'inner');
	    $this->db3->join('user_personal_info', 'user_profile.user_id = user_personal_info.user_id', 'inner');
	    $this->db3->where("user_profile.gender", "$gender");
	    if(!empty($age_from)){
	        $this->db3->where("TIMESTAMPDIFF(YEAR, FROM_UNIXTIME(user_profile.dob), NOW()) >=",$age_from);
	    }
	    if(!empty($age_to)){
	        $this->db3->where("TIMESTAMPDIFF(YEAR, FROM_UNIXTIME(user_profile.dob), NOW()) <=",$age_to);
	    }
	    if(!empty($religion)){
	        $this->db3->where("user_profile.religion_id", $religion);
	    }
	    $this->db3->where("users.status !=", 'D' );
	    $result['count']= $this->db3->count_all_results();
	    
	    return $result;
	}
	
	
	
	
	
	
	
}