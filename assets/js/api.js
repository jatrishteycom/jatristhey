jQuery(document).ready(function($){

const hostname=location.hostname;
const GETPROFILELIST = "/v1/api/profilefor";  // profie list
const GETCOUNTRYLIST = "/v1/api/countries";   // countries list
const GETRELIGION = "/v1/api/religion";   // religion list
const GETGOTRA = "/v1/api/gotra"; // Gotra list #http://jat.com/v1/api/gotra?religionid=2
const GETSTATE = "/v1/api/state"; //#state list http://jat.com/v1/api/state?countryid=83
const GETCITY = "/v1/api/city";   //#list of city http://jat.com/v1/api/city?stateid=83
const GETDISTRICT = "/v1/api/district";  //#http://jat.com/v1/api/district?stateid=33
const ADD_STEP1 = "/v1/api/submitstep1";  //#http://jat.com/v1/api/submitstep1
const LOGIN = "/v1/api/login"; // login

const X_EMAIL ="X-Email";
const X_PASSWORD = "X-Password";
const X_TOKEN = "X-Token";
const EMAILID = "abc@gmail.com";
const TOCKEN = "f85307a7c73640e75cfb9af4a325f5d1f3c1583d9916cf2565a0dd2e5919a6f7";
	
// get Profile list
	$.ajax({
		type: 'GET',
		headers: {
			"X-Email" :EMAILID,
			"X-Token" :TOCKEN,
	        'Content-Type':'application/json',
	    },
	    dataType: 'json',
		url: GETPROFILELIST,
		success: function(data){
			var options = '<option value="">Select Profile For</option>';
		      for (var i = 0; i < data['result'].length; i++) {
		        options += '<option value="' + data['result'][i]['id'] + '">' + data['result'][i]['name'] + '</option>';
		      }
			$(".profile_for").html(options);
		},
		error: function(result, errorType, errorMessage){
			var options = '<option value="">Select Profile For</option>';
			$(".profile_for").html(options);
        	console.log("Error:" + errorType + " " + errorMessage);
        }
	});
	
	
	/****** Select Religion*********/
	$.ajax({
		type: 'GET',
		headers: {
			"X-Email" :EMAILID,
			"X-Token" :TOCKEN,
	        'Content-Type':'application/json',
	    },
	    dataType: 'json',
		url: GETRELIGION,
		success: function(data){
			var options = '<option value="">Select Religion</option>';
		      for (var i = 0; i < data['result'].length; i++) {
		        options += '<option value="' + data['result'][i]['id'] + '">' + data['result'][i]['name'] + '</option>';
		      }
			$(".religion_list").html(options);
		},
		error: function(result, errorType, errorMessage){
			var options = '<option value="">Select Religion</option>';
			$(".religion_list").html(options);  
        	console.log("Error:" + errorType + " " + errorMessage);
        }
	});
	
	
//  For Gotr according to religion
	$(".religion_list").change(function(){
		var religion_id=$(".religion_list").val();
		$.ajax({
			type: 'GET',
			headers: {
				'X-Email':EMAILID,
				'X-Token':TOCKEN,
		        'Content-Type':'application/json',
		    },
		    dataType: 'json',
		    data : { religionid :+ religion_id  },
			url: GETGOTRA,
			success: function(data){
				var options = '<option value="">Select Gotra</option>';
			      for (var i = 0; i < data['result'].length; i++) {
			        options += '<option value="' + data['result'][i]['id'] + '">' + data['result'][i]['name'] + '</option>';
			      }
				$(".gotra_list").html(options);
			},
			error: function(result, errorType, errorMessage){
				var options = '<option value="">Select Gotra</option>';
				$(".gotra_list").html(options);  
	        	console.log("Error:" + errorType + " " + errorMessage);
	        }
		});
	});
	
	
	
	//  For Gotr according to religion
	$(".religion_list").change(function(){
		var religion_id=$(".religion_list").val();
		$.ajax({
			type: 'GET',
			headers: {
				'X-Email':EMAILID,
				'X-Token':TOCKEN,
		        'Content-Type':'application/json',
		    },
		    dataType: 'json',
		    data : { religionid :+ religion_id  },
			url: GETGOTRA,
			success: function(data){
				//alert(data);
				var options = '<option value="">Select Mother Gotra</option>';
			      for (var i = 0; i < data['result'].length; i++) {
			        options += '<option value="' + data['result'][i]['id'] + '">' + data['result'][i]['name'] + '</option>';
			      }
				$(".mother_gotra_list").html(options);
			},
			error: function(result, errorType, errorMessage){
				var options = '<option value="">Select Mother Gotra</option>';
				$(".mother_gotra_list").html(options);  
	        	console.log("Error:" + errorType + " " + errorMessage);
	        }
		});
	});


	
	$.ajax({
		type: 'GET',
		headers: {
			'X-Email':EMAILID,
			'X-Token':TOCKEN,
	        'Content-Type':'application/json',
	    },
	    dataType: 'json',
		url: GETCOUNTRYLIST,
		success: function(data){
			var options = '<option value="">Select Country</option>';
		      for (var i = 0; i < data['result'].length; i++) {
		        options += '<option value="' + data['result'][i]['country_id'] + '">' + data['result'][i]['Country'] + '</option>';
		      }
			$(".country_list").html(options);
		},
		error: function(result, errorType, errorMessage){
			//alert(result);
			//alert("nnnn");  
        	console.log("Error:" + errorType + " " + errorMessage);
        }
	});

	//  For State
	$(".country_list").change(function(){
		var country_id=$(".country_list").val();
		//alert(country_id);
		$.ajax({
			type: 'GET',
			headers: {
				'X-Email':EMAILID,
				'X-Token':TOCKEN,
		        'Content-Type':'application/json',
		    },
		    dataType: 'json',
		    data : { countryid :+ country_id  },
			url: GETSTATE,
			success: function(data){
				//alert(data);
				var options = '';
			      for (var i = 0; i < data['result'].length; i++) {
			        options += '<option value="' + data['result'][i]['state_id'] + '">' + data['result'][i]['State'] + '</option>';
			      }
				$(".state_list").html(options);
			},
			error: function(result, errorType, errorMessage){
				var options = '<option value="">Select State</option>';
				$(".state_list").html(options); 
	        	console.log("Error:" + errorType + " " + errorMessage);
	        }
		});
	});

	

//  For State
	$(".state_list").change(function(){
		var state_id=$(".state_list").val();
		//alert(country_id);
		$.ajax({
			type: 'GET',
			headers: {
				'X-Email':EMAILID,
				'X-Token':TOCKEN,
		        'Content-Type':'application/json',
		    },
		    dataType: 'json',
		    data : { stateid :+ state_id  },
			url: GETDISTRICT,
			success: function(data){
				//alert(data);
				var options = '';
			      for (var i = 0; i < data['result'].length; i++) {
			        options += '<option value="' + data['result'][i]['district_id'] + '">' + data['result'][i]['name'] + '</option>';
			      }
				$(".city_list").html(options);
			},
			error: function(result, errorType, errorMessage){
				var options = '<option value="">Select City</option>';
				$(".city_list").html(options); 
	        	console.log("Error:" + errorType + " " + errorMessage);
	        }
		});
	});


	
	
	
	
	
	
});