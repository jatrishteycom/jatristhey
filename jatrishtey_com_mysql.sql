-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: jatrishtey.com.mysql:3306
-- Generation Time: Nov 07, 2017 at 09:07 AM
-- Server version: 10.1.28-MariaDB-1~xenial
-- PHP Version: 5.4.45-0+deb7u11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jatrishtey_com_jatrishtey_com`
--
CREATE DATABASE `jatrishtey_com_jatrishtey_com` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `jatrishtey_com_jatrishtey_com`;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(10) NOT NULL AUTO_INCREMENT,
  `Country` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=206 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `Country`) VALUES
(1, 'Afghanistan'),
(2, 'Albania'),
(3, 'Algeria'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua and Barbuda'),
(7, 'Argentina'),
(8, 'Armenia'),
(9, 'Aruba'),
(10, 'Australia'),
(11, 'Austria'),
(12, 'Azerbaijan'),
(13, 'Bahamas, The'),
(14, 'Bahrain'),
(15, 'Bangladesh'),
(16, 'Barbados'),
(17, 'Belarus'),
(18, 'Belgium'),
(19, 'Belize'),
(20, 'Benin'),
(21, 'Bhutan'),
(22, 'Bolivia'),
(23, 'Bosnia and Herzegovina'),
(24, 'Botswana'),
(25, 'Brazil'),
(26, 'Brunei'),
(27, 'Bulgaria'),
(28, 'Burkina Faso'),
(29, 'Burma'),
(30, 'Burundi'),
(31, 'Cambodia'),
(32, 'Cameroon'),
(33, 'Canada'),
(34, 'Cabo Verde'),
(35, 'Central African Republic'),
(36, 'Chad'),
(37, 'Chile'),
(38, 'China'),
(39, 'Colombia'),
(40, 'Comoros'),
(41, 'Congo, Democratic Republic of the'),
(42, 'Congo, Republic of the'),
(43, 'Costa Rica'),
(44, 'Cote d''Ivoire'),
(45, 'Croatia'),
(46, 'Cuba'),
(47, 'Curacao'),
(48, 'Cyprus'),
(49, 'Czechia'),
(50, 'Denmark'),
(51, 'Djibouti'),
(52, 'Dominica'),
(53, 'Dominican Republic'),
(54, 'East Timor (see Timor-Leste)'),
(55, 'Ecuador'),
(56, 'Egypt'),
(57, 'El Salvador'),
(58, 'Equatorial Guinea'),
(59, 'Eritrea'),
(60, 'Estonia'),
(61, 'Ethiopia'),
(62, 'Fiji'),
(63, 'Finland'),
(64, 'France'),
(65, 'Top of Page'),
(66, 'Gabon'),
(67, 'Gambia, The'),
(68, 'Georgia'),
(69, 'Germany'),
(70, 'Ghana'),
(71, 'Greece'),
(72, 'Grenada'),
(73, 'Guatemala'),
(74, 'Guinea'),
(75, 'Guinea-Bissau'),
(76, 'Guyana'),
(77, 'Haiti'),
(78, 'Holy See'),
(79, 'Honduras'),
(80, 'Hong Kong'),
(81, 'Hungary'),
(82, 'Iceland'),
(83, 'India'),
(84, 'Indonesia'),
(85, 'Iran'),
(86, 'Iraq'),
(87, 'Ireland'),
(88, 'Israel'),
(89, 'Italy'),
(90, 'Jamaica'),
(91, 'Japan'),
(92, 'Jordan'),
(93, 'Kazakhstan'),
(94, 'Kenya'),
(95, 'Kiribati'),
(96, 'Korea, North'),
(97, 'Korea, South'),
(98, 'Kosovo'),
(99, 'Kuwait'),
(100, 'Kyrgyzstan'),
(101, 'Laos'),
(102, 'Latvia'),
(103, 'Lebanon'),
(104, 'Lesotho'),
(105, 'Liberia'),
(106, 'Libya'),
(107, 'Liechtenstein'),
(108, 'Lithuania'),
(109, 'Luxembourg'),
(110, 'Macau'),
(111, 'Macedonia'),
(112, 'Madagascar'),
(113, 'Malawi'),
(114, 'Malaysia'),
(115, 'Maldives'),
(116, 'Mali'),
(117, 'Malta'),
(118, 'Marshall Islands'),
(119, 'Mauritania'),
(120, 'Mauritius'),
(121, 'Mexico'),
(122, 'Micronesia'),
(123, 'Moldova'),
(124, 'Monaco'),
(125, 'Mongolia'),
(126, 'Montenegro'),
(127, 'Morocco'),
(128, 'Mozambique'),
(129, 'Namibia'),
(130, 'Nauru'),
(131, 'Nepal'),
(132, 'Netherlands'),
(133, 'New Zealand'),
(134, 'Nicaragua'),
(135, 'Niger'),
(136, 'Nigeria'),
(137, 'North Korea'),
(138, 'Norway'),
(139, 'Oman'),
(140, 'Pakistan'),
(141, 'Palau'),
(142, 'Palestinian Territories'),
(143, 'Panama'),
(144, 'Papua New Guinea'),
(145, 'Paraguay'),
(146, 'Peru'),
(147, 'Philippines'),
(148, 'Poland'),
(149, 'Portugal'),
(150, 'Qatar'),
(151, 'Romania'),
(152, 'Russia'),
(153, 'Rwanda'),
(154, 'Saint Kitts and Nevis'),
(155, 'Saint Lucia'),
(156, 'Saint Vincent and the Grenadines'),
(157, 'Samoa'),
(158, 'San Marino'),
(159, 'Sao Tome and Principe'),
(160, 'Saudi Arabia'),
(161, 'Senegal'),
(162, 'Serbia'),
(163, 'Seychelles'),
(164, 'Sierra Leone'),
(165, 'Singapore'),
(166, 'Sint Maarten'),
(167, 'Slovakia'),
(168, 'Slovenia'),
(169, 'Solomon Islands'),
(170, 'Somalia'),
(171, 'South Africa'),
(172, 'South Korea'),
(173, 'South Sudan'),
(174, 'Spain'),
(175, 'Sri Lanka'),
(176, 'Sudan'),
(177, 'Suriname'),
(178, 'Swaziland'),
(179, 'Sweden'),
(180, 'Switzerland'),
(181, 'Syria'),
(182, 'Taiwan'),
(183, 'Tajikistan'),
(184, 'Tanzania'),
(185, 'Thailand'),
(186, 'Timor-Leste'),
(187, 'Togo'),
(188, 'Tonga'),
(189, 'Trinidad and Tobago'),
(190, 'Tunisia'),
(191, 'Turkey'),
(192, 'Turkmenistan'),
(193, 'Tuvalu'),
(194, 'Uganda'),
(195, 'Ukraine'),
(196, 'United Arab Emirates'),
(197, 'United Kingdom'),
(198, 'Uruguay'),
(199, 'Uzbekistan'),
(200, 'Vanuatu'),
(201, 'Venezuela'),
(202, 'Vietnam'),
(203, 'Yemen'),
(204, 'Zambia'),
(205, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `district_id` int(10) NOT NULL AUTO_INCREMENT,
  `StateID` bigint(20) NOT NULL DEFAULT '0',
  `CountryID` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`district_id`, `StateID`, `CountryID`, `name`) VALUES
(1, 33, 0, 'Saharanpur'),
(2, 33, 0, 'Muzaffarnagar'),
(3, 33, 0, 'Bulandshahar'),
(4, 33, 0, 'Ghaziabad'),
(5, 33, 0, 'Meerut');

-- --------------------------------------------------------

--
-- Table structure for table `gotra`
--

CREATE TABLE IF NOT EXISTS `gotra` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `relegionrefid` int(9) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=358 ;

--
-- Dumping data for table `gotra`
--

INSERT INTO `gotra` (`id`, `relegionrefid`, `name`) VALUES
(1, 2, 'Abusaria'),
(2, 2, 'Achara'),
(3, 2, 'Ahlawat'),
(4, 2, 'Agre'),
(5, 2, 'Ajmeria'),
(6, 2, 'Andhak'),
(7, 2, 'Anaadi'),
(8, 2, 'Antal'),
(9, 2, 'Asiagh'),
(10, 2, 'Atri'),
(11, 2, 'Atwal'),
(12, 2, 'Aulakh'),
(13, 2, 'Aujla'),
(14, 2, 'Agah'),
(15, 2, 'Babal'),
(16, 2, 'Bachhal'),
(17, 2, 'Badesha'),
(18, 2, 'Badyal'),
(19, 2, 'Bhatoa'),
(20, 2, 'Bagri'),
(21, 2, 'Bahia'),
(22, 2, 'Baht'),
(23, 2, 'Baidwan'),
(24, 2, 'Bains'),
(25, 2, 'Bajwa'),
(26, 2, 'Bajya'),
(27, 2, 'Balhara'),
(28, 2, 'Balyan'),
(29, 2, 'Bamraulia'),
(30, 2, 'Bana'),
(31, 2, 'Bansi'),
(32, 2, 'Barjati'),
(33, 2, 'Barola'),
(34, 2, 'Basra'),
(35, 2, 'Basran'),
(36, 2, 'Baswan'),
(37, 2, 'Bassi'),
(38, 2, 'Batar'),
(39, 2, 'Beniwal'),
(40, 2, 'Benning'),
(41, 2, 'Bhadare'),
(42, 2, 'Bhadiar'),
(43, 2, 'Bhadu'),
(44, 2, 'Bhalothia'),
(45, 2, 'Bhalli'),
(46, 2, 'Bhambu'),
(47, 2, 'Bhangu/Bhangoo'),
(48, 2, 'Bharhaich'),
(49, 2, 'Bhari'),
(50, 2, 'Bhela'),
(51, 2, 'Bhattal'),
(52, 2, 'Bhichar'),
(53, 2, 'Bhind'),
(54, 2, 'Bhukar'),
(55, 2, 'Bhullar'),
(56, 2, 'Billing'),
(57, 2, 'Brar'),
(58, 2, 'Braich'),
(59, 2, 'Budania'),
(60, 2, 'Budhwar'),
(61, 2, 'Burdak'),
(62, 2, 'Buttar'),
(63, 2, 'Chadhar'),
(64, 2, 'Chahal'),
(65, 2, 'Chahar'),
(66, 2, 'Chauhan'),
(67, 2, 'Chandel'),
(68, 2, 'Chikkara'),
(69, 2, 'Cheema'),
(70, 2, 'Chhillar'),
(71, 2, 'Chheena'),
(72, 2, 'Chaudhary'),
(73, 2, 'Chaitha'),
(74, 2, 'Dabas'),
(75, 2, 'Dabra'),
(76, 2, 'Dagur'),
(77, 2, 'Dahiya'),
(78, 2, 'Dandiwal'),
(79, 2, 'Dalal'),
(80, 2, 'Dangi'),
(81, 2, 'Deo'),
(82, 2, 'Deol'),
(83, 2, 'Deshwal'),
(84, 2, 'Dhariwal'),
(85, 2, 'Dhesi'),
(86, 2, 'Dhaliwal'),
(87, 2, 'Dhankhar'),
(88, 2, 'Dhadli'),
(89, 2, 'Dhanoa'),
(90, 2, 'Dhama'),
(91, 2, 'Dharan'),
(92, 2, 'Dharni'),
(93, 2, 'Dhatarwal'),
(94, 2, 'Dhatt'),
(95, 2, 'Dhaulya'),
(96, 2, 'Dhaurelia'),
(97, 2, 'Dhillon'),
(98, 2, 'Dhindawal'),
(99, 2, 'Dhindsa'),
(100, 2, 'Dholia'),
(101, 2, 'Dhoot'),
(102, 2, 'Dosanjh'),
(103, 2, 'Dudi'),
(104, 2, 'Duhan'),
(105, 2, 'Dullar'),
(106, 2, 'Fageria'),
(107, 2, 'Fandan'),
(108, 2, 'Farswal'),
(109, 2, 'Faugat'),
(110, 2, 'Faujdar'),
(111, 2, 'Garcha'),
(112, 2, 'Gahlot'),
(113, 2, 'Gandhar'),
(114, 2, 'Ghatwala'),
(115, 2, 'Garewal'),
(116, 2, 'Ghumman'),
(117, 2, 'Gill'),
(118, 2, 'Gauria'),
(119, 2, 'Gehlawat'),
(120, 2, 'Godara'),
(121, 2, 'Ghangas'),
(122, 2, 'Ghick'),
(123, 2, 'Gora'),
(124, 2, 'Goraya'),
(125, 2, 'Gosal'),
(126, 2, 'Grewal'),
(127, 2, 'Gulia'),
(128, 2, 'Guram'),
(129, 2, 'Gurm'),
(130, 2, 'Ghugh'),
(131, 2, 'Hala'),
(132, 2, 'Hanga'),
(133, 2, 'Hayer'),
(134, 2, 'Heer'),
(135, 2, 'Hooda'),
(136, 2, 'Hundal'),
(137, 2, 'Indolia'),
(138, 2, 'Jauhal'),
(139, 2, 'Jakhar'),
(140, 2, 'Jaglan'),
(141, 2, 'Janghu'),
(142, 2, 'Janu'),
(143, 2, 'Jatasra'),
(144, 2, 'Jatrana'),
(145, 2, 'Jatri'),
(146, 2, 'Jawanda'),
(147, 2, 'Jhajharia'),
(148, 2, 'Jhammat'),
(149, 2, 'Jhutti'),
(150, 2, 'Johal'),
(151, 2, 'Johiya'),
(152, 2, 'Joon'),
(153, 2, 'Jagpal'),
(154, 2, 'Jhinjar'),
(155, 2, 'Kahlon'),
(156, 2, 'Kadian'),
(157, 2, 'Kajala'),
(158, 2, 'Kakran'),
(159, 2, 'Kak'),
(160, 2, 'Kaler'),
(161, 2, 'Kalirai'),
(162, 2, 'Kalkat'),
(163, 2, 'Kalkhande'),
(164, 2, 'Kandhola'),
(165, 2, 'Kang'),
(166, 2, 'Karwasra'),
(167, 2, 'Kisana'),
(168, 2, 'Kaswan'),
(169, 2, 'Kataria'),
(170, 2, 'Katewa'),
(171, 2, 'Kehal'),
(172, 2, 'khangura'),
(173, 2, 'Khainwar'),
(174, 2, 'Khakh'),
(175, 2, 'Khalia'),
(176, 2, 'Kharb'),
(177, 2, 'Khatri'),
(178, 2, 'Khehra'),
(179, 2, 'Kherwa'),
(180, 2, 'Khichad'),
(181, 2, 'Khirwar'),
(182, 2, 'Khinger'),
(183, 2, 'Khokhar'),
(184, 2, 'Khosa'),
(185, 2, 'Khoye Maurya'),
(186, 2, 'Kooner'),
(187, 2, 'Kuhar'),
(188, 2, 'Kular'),
(189, 2, 'Kularia'),
(190, 2, 'Kulhari'),
(191, 2, 'Kundu'),
(192, 2, 'Kuntal'),
(193, 2, 'Lally'),
(194, 2, 'Lalli'),
(195, 2, 'Lakra'),
(196, 2, 'Lather'),
(197, 2, 'Langrial'),
(198, 2, 'Lakhlan'),
(199, 2, 'Lakhan'),
(200, 2, 'Lengha'),
(201, 2, 'Liddar'),
(202, 2, 'Lochab'),
(203, 2, 'Lathwal'),
(204, 2, 'Maan'),
(205, 2, 'Mandhan'),
(206, 2, 'Manes'),
(207, 2, 'Madrak'),
(208, 2, 'Mahal'),
(209, 2, 'Malik'),
(210, 2, 'Mandeer'),
(211, 2, 'Mander'),
(212, 2, 'Munder'),
(213, 2, 'Mandiwal'),
(214, 2, 'Mangat'),
(215, 2, 'Mann'),
(216, 2, 'Mundi'),
(217, 2, 'Mungut'),
(218, 2, 'Mede'),
(219, 2, 'Meel'),
(220, 2, 'Mehria'),
(221, 2, 'Maichu'),
(222, 2, 'Mohar'),
(223, 2, 'Monga'),
(224, 2, 'Moonga'),
(225, 2, 'Moond'),
(226, 2, 'Motsara'),
(227, 2, 'Mahawal'),
(228, 2, 'Maulia'),
(229, 2, 'Mahawalia'),
(230, 2, 'Mohil'),
(231, 2, 'Naga'),
(232, 2, 'Nagra'),
(233, 2, 'Nagauria'),
(234, 2, 'Nahl'),
(235, 2, 'Nain'),
(236, 2, 'Nandal'),
(237, 2, 'Nantaal'),
(238, 2, 'Natt'),
(239, 2, 'Nauhwar'),
(240, 2, 'Nehra'),
(241, 2, 'Nijjar'),
(242, 2, 'Nitharwal'),
(243, 2, 'Noon'),
(244, 2, 'Ohlan'),
(245, 2, 'Ola'),
(246, 2, 'Pachar'),
(247, 2, 'Pachehra'),
(248, 2, 'Padda'),
(249, 2, 'Palsania'),
(250, 2, 'Palrwal'),
(251, 2, 'Panaich'),
(252, 2, 'Panghal'),
(253, 2, 'Parihar'),
(254, 2, 'Pandher'),
(255, 2, 'Pangli'),
(256, 2, 'Pannu'),
(257, 2, 'Pansota'),
(258, 2, 'Pawar'),
(259, 2, 'Panwar'),
(260, 2, 'Phalaswal'),
(261, 2, 'Phogat'),
(262, 2, 'Pilania'),
(263, 2, 'Punia'),
(264, 2, 'Punial'),
(265, 2, 'Punian'),
(266, 2, 'Purwar'),
(267, 2, 'Purewal'),
(268, 2, 'Pooni'),
(269, 2, 'Poria'),
(270, 2, 'Potaysir'),
(271, 2, 'Rai'),
(272, 2, 'Rajawat'),
(273, 2, 'Rajian'),
(274, 2, 'Rajaura'),
(275, 2, 'Rana'),
(276, 2, 'Rakkar'),
(277, 2, 'Ranu'),
(278, 2, 'Ranwa'),
(279, 2, 'Rathi'),
(280, 2, 'Rasoda'),
(281, 2, 'Rawala'),
(282, 2, 'Rehal'),
(283, 2, 'Redhu'),
(284, 2, 'Repswal'),
(285, 2, 'Rhind-Tutt'),
(286, 2, 'Riar'),
(287, 2, 'Romana'),
(288, 2, 'Rulania'),
(289, 2, 'Rupal'),
(290, 2, 'Randhawa'),
(291, 2, 'Sabharwal'),
(292, 2, 'Sahota'),
(293, 2, 'Saharan'),
(294, 2, 'Samra'),
(295, 2, 'Sandhu'),
(296, 2, 'Sangwan'),
(297, 2, 'Sanghera'),
(298, 2, 'Saroha'),
(299, 2, 'Sran'),
(300, 2, 'Sra'),
(301, 2, 'Sehrawat'),
(302, 2, 'Seen'),
(303, 2, 'Sehwag'),
(304, 2, 'sejwal'),
(305, 2, 'Sekhon'),
(306, 2, 'Seoran'),
(307, 2, 'Sheoran'),
(308, 2, 'Shergill'),
(309, 2, 'Shokeen'),
(310, 2, 'Seokhand'),
(311, 2, 'Sidhu'),
(312, 2, 'Sigroha'),
(313, 2, 'Sikarwar'),
(314, 2, 'Sinsinwar'),
(315, 2, 'Sansanwal'),
(316, 2, 'Sirohi'),
(317, 2, 'SialSiwach'),
(318, 2, 'Sunda'),
(319, 2, 'Soban'),
(320, 2, 'Solanki'),
(321, 2, 'Sohal'),
(322, 2, 'Sohi'),
(323, 2, 'Sumal'),
(324, 2, 'Tharoda'),
(325, 2, 'Teerwal'),
(326, 2, 'Takhar'),
(327, 2, 'Tanwar'),
(328, 2, 'Tarar'),
(329, 2, 'Tatla'),
(330, 2, 'Tatran'),
(331, 2, 'Takshak'),
(332, 2, 'Tevatia'),
(333, 2, 'Thenua'),
(334, 2, 'Thakran'),
(335, 2, 'Thandi'),
(336, 2, 'Thathiala'),
(337, 2, 'Thind'),
(338, 2, 'Thori'),
(339, 2, 'Tiwana'),
(340, 2, 'Tokas'),
(341, 2, 'Tomara'),
(342, 2, 'Tomar'),
(343, 2, 'Toor'),
(344, 2, 'Tung'),
(345, 2, 'Tushir'),
(346, 2, 'Toot'),
(347, 2, 'Uppal'),
(348, 2, 'Udar'),
(349, 2, 'Ujjwal'),
(350, 2, 'Vanar'),
(351, 2, 'Virk'),
(352, 2, 'Vaince'),
(353, 2, 'Vijayrania'),
(354, 2, 'Wahla'),
(355, 2, 'Warraich'),
(356, 2, 'Waraich'),
(357, 2, 'Wainse');

-- --------------------------------------------------------

--
-- Table structure for table `profile_for`
--

CREATE TABLE IF NOT EXISTS `profile_for` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `profile_for`
--

INSERT INTO `profile_for` (`id`, `name`) VALUES
(1, 'Myself'),
(2, 'Son'),
(3, 'Daughter'),
(4, 'Brother'),
(5, 'Sister'),
(6, 'Relative'),
(7, 'Friend');

-- --------------------------------------------------------

--
-- Table structure for table `religion`
--

CREATE TABLE IF NOT EXISTS `religion` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `religion`
--

INSERT INTO `religion` (`id`, `name`) VALUES
(1, 'Muslim'),
(2, 'Hindu'),
(3, 'Sikh');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `state_id` int(10) NOT NULL AUTO_INCREMENT,
  `CountryID` bigint(20) NOT NULL DEFAULT '0',
  `State` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=350 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `CountryID`, `State`) VALUES
(1, 83, 'Andaman and Nicobar'),
(2, 83, 'Andhra Pradesh'),
(3, 83, 'Arunachal Pradesh'),
(4, 83, 'Assam'),
(5, 83, 'Bihar'),
(6, 83, 'Chandigarh'),
(7, 83, 'Chhattisgarh'),
(8, 83, 'Dadra and Nagar Haveli'),
(9, 83, 'Daman and Diu'),
(10, 83, 'Delhi'),
(11, 83, 'Goa'),
(12, 83, 'Gujarat'),
(13, 83, 'Haryana'),
(14, 83, 'Himachal Pradesh'),
(15, 83, 'Jammu and Kashmir'),
(16, 83, 'Jharkhand'),
(17, 83, 'Karnataka'),
(18, 83, 'Kerala'),
(19, 83, 'Lakshadweep'),
(20, 83, 'Madhya Pradesh'),
(21, 83, 'Maharashtra'),
(22, 83, 'Manipur'),
(23, 83, 'Meghalaya'),
(24, 83, 'Mizoram'),
(25, 83, 'Nagaland'),
(26, 83, 'Orissa'),
(27, 83, 'Puducherry'),
(28, 83, 'Punjab'),
(29, 83, 'Rajasthan'),
(30, 83, 'Sikkim'),
(31, 83, 'Tamil Nadu'),
(32, 83, 'Tripura'),
(33, 83, 'Uttar Pradesh'),
(34, 83, 'Uttarakhand'),
(35, 83, 'West Benga'),
(36, 2, 'Alabama'),
(37, 2, 'Alaska'),
(38, 2, 'Arizona'),
(39, 2, 'Arkansas'),
(40, 2, 'California'),
(41, 2, 'Colorado'),
(42, 2, 'Connecticut'),
(43, 2, 'Delaware'),
(44, 2, 'District of Columbia'),
(45, 2, 'Florida'),
(46, 2, 'Georgia'),
(47, 2, 'Hawaii'),
(48, 2, 'Idaho'),
(49, 2, 'Illinois'),
(50, 2, 'Indiana'),
(51, 2, 'Iowa'),
(52, 2, 'Kansas'),
(53, 2, 'Kentucky'),
(54, 2, 'Louisiana'),
(55, 2, 'Maine'),
(56, 2, 'Maryland'),
(57, 2, 'Massachusetts'),
(58, 2, 'Michigan'),
(59, 2, 'Minnesota'),
(60, 2, 'Mississippi'),
(61, 2, 'Missouri'),
(62, 2, 'Montana'),
(63, 2, 'Nebraska'),
(64, 2, 'Nevada'),
(65, 2, 'New Hampshire'),
(66, 2, 'New Jersey'),
(67, 2, 'New Mexico'),
(68, 2, 'New York'),
(69, 2, 'North Carolina'),
(70, 2, 'North Dakota'),
(71, 2, 'Ohio'),
(72, 2, 'Oklahoma'),
(73, 2, 'Oregon'),
(74, 2, 'Pennsylvania'),
(75, 2, 'Rhode Island'),
(76, 2, ''),
(77, 2, 'South Carolina'),
(78, 2, 'South Carolina'),
(79, 2, 'South Dakota'),
(80, 2, 'Tennessee'),
(81, 2, 'Texas'),
(82, 2, 'Utah'),
(83, 2, 'Vermont'),
(84, 2, 'Virginia'),
(85, 2, 'Washington'),
(86, 2, 'West Virginia'),
(87, 2, 'Wisconsin'),
(88, 2, 'Wyoming'),
(89, 3, 'England'),
(90, 3, 'Northern Ireland'),
(91, 3, 'Scotland'),
(92, 3, 'Wales'),
(93, 4, 'Alberta'),
(94, 4, 'British Columbia'),
(95, 4, 'Labrado'),
(96, 4, 'Manitoba'),
(97, 4, 'New Brunswick'),
(98, 4, 'Newfoundland'),
(99, 4, 'Nova Scotia'),
(100, 4, 'Ontario'),
(101, 4, 'Prince Edward Island'),
(102, 4, 'Quebec'),
(103, 4, 'Saskatchewan'),
(104, 4, 'ACT'),
(105, 5, 'ACT'),
(106, 5, 'New South Wales'),
(107, 5, 'Northern Territory'),
(108, 5, 'Queensland'),
(109, 5, 'South Australia'),
(110, 5, 'Tasmania'),
(111, 5, 'Victoria'),
(112, 5, 'Western Australia'),
(113, 6, 'Albania'),
(114, 6, 'Andorra'),
(115, 6, 'Armenia'),
(116, 6, 'Austria'),
(117, 6, 'Belarus'),
(118, 6, 'Belgium'),
(119, 6, 'Bosnia'),
(120, 6, 'Bulgaria'),
(121, 6, 'Croatia'),
(122, 6, 'Cyprus'),
(123, 6, 'Czech Republic'),
(124, 6, 'Denmark'),
(125, 6, 'Estonia'),
(126, 6, 'Faroe Islands'),
(127, 6, 'Finland'),
(128, 6, 'France'),
(129, 6, 'Georgia'),
(130, 6, 'Germany'),
(131, 6, 'Gibraltar'),
(132, 6, 'Greece'),
(133, 6, 'Greenland'),
(134, 6, 'Holland'),
(135, 6, 'Hungary'),
(136, 6, 'Iceland'),
(137, 6, 'Ireland'),
(138, 6, 'Italy'),
(139, 6, 'Latvia'),
(140, 6, 'Liechtenstein'),
(141, 6, 'Lithuania'),
(142, 6, 'Luxembourg'),
(143, 6, 'Macedonia'),
(144, 6, 'Malta'),
(145, 6, 'Moldova'),
(146, 6, 'Monaco'),
(147, 6, 'Montenegro'),
(148, 6, 'Norway'),
(149, 6, 'Poland'),
(150, 6, 'Portugal'),
(151, 6, 'Romania'),
(152, 6, 'Russia'),
(153, 6, 'San Marino'),
(154, 6, 'Serbia'),
(155, 6, 'Slovakia'),
(156, 6, 'Slovenia'),
(157, 6, 'Spain'),
(158, 6, 'Svalbard'),
(159, 6, 'Sweden'),
(160, 6, 'Switzerland'),
(161, 6, 'Turkey'),
(162, 6, 'Ukraine'),
(163, 6, 'Vatican City'),
(164, 7, 'Afghanistan'),
(165, 7, 'Azerbaijan'),
(166, 7, 'Bangladesh'),
(167, 7, 'Bhutan'),
(168, 7, 'Brunei'),
(169, 7, 'Cambodia'),
(170, 7, 'China'),
(171, 7, 'Hong Kong'),
(172, 7, 'Indonesia'),
(173, 7, 'Japan'),
(174, 7, 'Kazakhstan'),
(175, 7, 'Kyrgyzstan'),
(176, 7, 'Laos'),
(177, 7, 'Macau'),
(178, 7, 'Malaysia'),
(179, 7, 'Maldives'),
(180, 7, 'Mongolia'),
(181, 7, 'Myanmar'),
(182, 7, 'Nepal'),
(183, 7, 'North Korea'),
(184, 7, 'Pakistan'),
(185, 7, 'Paracel Islands'),
(186, 7, 'Philippines'),
(187, 7, 'Singapore'),
(188, 7, 'South Korea'),
(189, 7, 'Spratly Islands'),
(190, 7, 'Sri Lanka'),
(191, 7, 'Taiwan'),
(192, 7, 'Tajikistan'),
(193, 7, 'Thailand'),
(194, 7, 'Turkey'),
(195, 7, 'Turkmenistan'),
(196, 7, 'Uzbekistan'),
(197, 7, 'Vietnam'),
(198, 8, 'Bahrain'),
(199, 8, 'Iran'),
(200, 8, 'Iraq'),
(201, 8, 'Israel'),
(202, 8, 'Jordan'),
(203, 8, 'Kuwait'),
(204, 8, 'Lebanon'),
(205, 8, 'Oman'),
(206, 8, 'Qatar'),
(207, 8, 'Saudi Arabia'),
(208, 8, 'Syria'),
(209, 8, 'UAE'),
(210, 8, 'West Bank'),
(211, 8, 'Yemen'),
(212, 9, 'Algeria'),
(213, 9, 'Angola'),
(214, 9, 'Benin'),
(215, 9, 'Botswana'),
(216, 9, 'Burkina Faso'),
(217, 9, 'Burundi'),
(218, 9, 'Cameroon'),
(219, 9, 'Cape Verde'),
(220, 9, 'Cen. Afr. Rep.'),
(221, 9, 'Chad'),
(222, 9, 'Comoros'),
(223, 9, 'Congo'),
(224, 9, 'Djibouti'),
(225, 9, 'Egypt'),
(226, 9, 'Eq. Guinea'),
(227, 9, 'Eritrea'),
(228, 9, 'Ethiopia'),
(229, 9, 'Gabon'),
(230, 9, 'Ghana'),
(231, 9, 'Guinea'),
(232, 9, 'Guinea Bissau'),
(233, 9, 'Ivory Coast'),
(234, 9, 'Kenya'),
(235, 9, 'Lesotho'),
(236, 9, 'Liberia'),
(237, 9, 'Libya'),
(238, 9, 'Madagascar'),
(239, 9, 'Malawi'),
(240, 9, 'Mali'),
(241, 9, 'Mauritania'),
(242, 9, 'Mauritius'),
(243, 9, 'Morocco'),
(244, 9, 'Mozambique'),
(245, 9, 'Namibia'),
(246, 9, 'Niger'),
(247, 9, 'Nigeria'),
(248, 9, 'Reunion'),
(249, 9, 'Rwanda'),
(250, 9, 'Saint Helena'),
(251, 9, 'Sao Tome'),
(252, 9, 'Senegal'),
(253, 9, 'Seychelles'),
(254, 9, 'Sierra Leone'),
(255, 9, 'Somalia'),
(256, 9, 'South Africa'),
(257, 9, 'Sudan'),
(258, 9, 'Swaziland'),
(259, 9, 'Tanzania'),
(260, 9, 'The Gambia'),
(261, 9, 'Togo'),
(262, 9, 'Tunisia'),
(263, 9, ''),
(264, 9, 'Uganda'),
(265, 9, 'W. Sahara'),
(266, 9, 'Zaire'),
(267, 9, 'Zambia'),
(268, 9, 'Zimbabwe'),
(269, 10, 'Anguilla'),
(270, 10, 'Antigua'),
(271, 10, 'Aruba'),
(272, 10, 'Bahamas'),
(273, 10, 'Barbados'),
(274, 10, 'Belize'),
(275, 10, 'Bermuda'),
(276, 10, 'British V.I.'),
(277, 10, 'Cayman Islands'),
(278, 10, 'Colombia'),
(279, 10, 'Costa Rica'),
(280, 10, 'Cuba'),
(281, 10, 'Dom. Republic'),
(282, 10, 'Dominica'),
(283, 10, 'Dutch Antilles'),
(284, 10, 'El Salvador'),
(285, 10, 'Grenada'),
(286, 10, 'Grenadines'),
(287, 10, 'Guadeloupe'),
(288, 10, 'Haiti'),
(289, 10, 'Jamaica'),
(290, 10, 'Martinique'),
(291, 10, 'Mexico'),
(292, 10, 'Panama'),
(293, 10, 'Puerto Rico'),
(294, 10, 'St. Barths'),
(295, 10, 'St. Kitts'),
(296, 10, 'St. Lucia'),
(297, 10, 'St. Martin'),
(298, 10, 'St. Vincent'),
(299, 10, 'Trinidad and Tobago'),
(300, 10, 'Turks and Caicos'),
(301, 10, 'US Virgin Islands'),
(302, 10, 'Venezuela'),
(303, 11, 'American Samoa'),
(304, 11, 'Cook Islands'),
(305, 11, 'East Timor'),
(306, 11, 'Fiji'),
(307, 11, 'French Polynesia'),
(308, 11, 'Guam'),
(309, 11, 'Kiribati'),
(310, 11, 'Mariana Islands'),
(311, 11, 'Marshall Islands'),
(312, 11, 'Micronesia'),
(313, 11, 'Nauru'),
(314, 11, 'New Caledonia'),
(315, 11, 'New Zealand'),
(316, 11, 'Palau'),
(317, 11, 'Pap. New Guinea'),
(318, 11, 'Pitcairn Islands'),
(319, 11, 'Samoa (Western)'),
(320, 11, 'Solomon Isles'),
(321, 11, 'Tokelau'),
(322, 11, 'Tonga'),
(323, 11, 'Tuvalu'),
(324, 11, 'Vanuatu'),
(325, 11, 'Wallis And Futuna'),
(326, 11, 'Argentina'),
(327, 11, 'Belize'),
(328, 12, 'Belize'),
(329, 12, 'Bolivia'),
(330, 12, 'Brazil'),
(331, 12, 'Chile'),
(332, 12, 'Colombia'),
(333, 12, 'Costa Rica'),
(334, 12, 'Ecuador'),
(335, 12, 'El Salvador'),
(336, 12, 'Falklands'),
(337, 12, 'Fr. Guiana'),
(338, 12, 'Guatemala'),
(339, 12, 'Guyana'),
(340, 12, 'Honduras'),
(341, 12, 'Mexico'),
(342, 12, 'Nicaragua'),
(343, 12, 'Panama'),
(344, 12, 'Paraguay'),
(345, 12, 'Peru'),
(346, 12, 'Sth. Georgia'),
(347, 12, 'Suriname'),
(348, 12, 'Uruguay'),
(349, 12, 'Venezuela');

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE IF NOT EXISTS `user_profile` (
  `profile_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to users table',
  `religion_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to religion table',
  `gotra_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to gotra table',
  `state_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to states table',
  `country_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to countries table',
  `district_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to dostricts table',
  `fname` varchar(32) DEFAULT NULL,
  `lname` varchar(32) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL COMMENT 'M=Male,F=Female',
  `father_name` varchar(50) DEFAULT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `refrence_name` varchar(50) DEFAULT NULL,
  `refrence_number` varchar(16) DEFAULT NULL,
  `mobile` varchar(16) DEFAULT NULL,
  `dob` int(10) DEFAULT '0',
  `created` int(10) DEFAULT '0',
  `created_by` int(10) DEFAULT '0',
  `updated` int(10) DEFAULT '0',
  `updated_by` int(10) DEFAULT '0',
  `mother_gotra` int(10) DEFAULT '0',
  `reg_step` int(10) DEFAULT '0',
  PRIMARY KEY (`profile_id`),
  KEY `user_id` (`user_id`),
  KEY `religion_id` (`religion_id`),
  KEY `gotra_id` (`gotra_id`),
  KEY `state_id` (`state_id`),
  KEY `country_id` (`country_id`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`profile_id`, `user_id`, `religion_id`, `gotra_id`, `state_id`, `country_id`, `district_id`, `fname`, `lname`, `gender`, `father_name`, `mother_name`, `refrence_name`, `refrence_number`, `mobile`, `dob`, `created`, `created_by`, `updated`, `updated_by`, `mother_gotra`, `reg_step`) VALUES
(6, 7, 1, 1, 1, 1, 1, 'xxx', 'yyy', 'F', 'ddddd', 'fggggg', 'fdfdfd', '555555', '+91-81728254052', 574799400, 1509131852, 7, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `status` enum('A','I','H','D') DEFAULT NULL COMMENT 'A=Active I=Inactive H=Hold D=Deleted',
  `accesstype` enum('admin','user') DEFAULT NULL,
  `usertype` enum('P','G','N') DEFAULT NULL COMMENT 'P=Premium G=Gold N=Normal',
  `email` varchar(45) DEFAULT NULL,
  `token` varchar(99) DEFAULT NULL,
  `token_expire` int(10) DEFAULT '0',
  `created` int(10) DEFAULT '0',
  `created_by` int(10) DEFAULT '0',
  `updated` int(10) DEFAULT '0',
  `updated_by` int(10) DEFAULT '0',
  `experi_time` int(10) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `status`, `accesstype`, `usertype`, `email`, `token`, `token_expire`, `created`, `created_by`, `updated`, `updated_by`, `experi_time`) VALUES
(7, '+91-81728254052', 'e10adc3949ba59abbe56e057f20f883e', 'H', 'user', 'P', 'hash@gmail.com', 'b305e9e24ecdc5bbc31eba90afdb13b9', 0, 1509131852, 0, 0, 0, 1509218252);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD CONSTRAINT `user_profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `user_profile_ibfk_2` FOREIGN KEY (`religion_id`) REFERENCES `religion` (`id`),
  ADD CONSTRAINT `user_profile_ibfk_3` FOREIGN KEY (`gotra_id`) REFERENCES `gotra` (`id`),
  ADD CONSTRAINT `user_profile_ibfk_4` FOREIGN KEY (`state_id`) REFERENCES `states` (`state_id`),
  ADD CONSTRAINT `user_profile_ibfk_6` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`),
  ADD CONSTRAINT `user_profile_ibfk_7` FOREIGN KEY (`district_id`) REFERENCES `districts` (`district_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


CREATE TABLE IF NOT EXISTS education (
  id int(5) NOT NULL AUTO_INCREMENT,
  name varchar(90) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


insert into education(name) values('Bachelors in Engineering / Computers');
insert into education(name) values('Masters in Engineering / Computers');
insert into education(name) values('Bachelors in Arts / Science / Commerce');
insert into education(name) values('Bachelors in Management');
insert into education(name) values('Bachelors in Medicine in General / Dental / Surgeon');
insert into education(name) values('Bachelors in Legal');
insert into education(name) values('Higher Secondary / Secondary');
insert into education(name) values('Any Diploma');
insert into education(name) values('Ph.D.');
insert into education(name) values('Masters in Legal');
insert into education(name) values('Masters in Medicine in General / Dental / Surgeon');
insert into education(name) values('Masters in Management');
insert into education(name) values('Masters in Arts / Science / Commerce');
insert into education(name) values('Other');


CREATE TABLE IF NOT EXISTS heightgroombride (
  id int(5) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


insert into heightgroombride(name) values('4ft - 121 cm');
insert into heightgroombride(name) values('4ft 1in - 124cm');
insert into heightgroombride(name) values('4ft 2in - 127cm');
insert into heightgroombride(name) values('4ft 3in - 129cm');
insert into heightgroombride(name) values('4ft 4in - 132cm');
insert into heightgroombride(name) values('4ft 5in - 134cm');
insert into heightgroombride(name) values('4ft 6in - 137cm');
insert into heightgroombride(name) values('4ft 7in - 139cm');
insert into heightgroombride(name) values('4ft 8in - 142cm');
insert into heightgroombride(name) values('4ft 9in - 144cm');
insert into heightgroombride(name) values('4ft 10in - 147cm');
insert into heightgroombride(name) values('4ft 11in - 149cm');
insert into heightgroombride(name) values('5ft - 152cm');
insert into heightgroombride(name) values('5ft 1in - 154cm');
insert into heightgroombride(name) values('5ft 2in - 157cm');
insert into heightgroombride(name) values('5ft 3in - 160cm');
insert into heightgroombride(name) values('5ft 4in - 162cm');
insert into heightgroombride(name) values('5ft 5in - 165cm');
insert into heightgroombride(name) values('5ft 6in - 167cm');
insert into heightgroombride(name) values('5ft 7in - 170cm');
insert into heightgroombride(name) values('5ft 8in - 172cm');
insert into heightgroombride(name) values('5ft 9in - 175cm');
insert into heightgroombride(name) values('5ft 10in - 177cm');
insert into heightgroombride(name) values('5ft 11in - 180cm');
insert into heightgroombride(name) values('6ft - 182cm');
insert into heightgroombride(name) values('6ft 1in - 185cm');
insert into heightgroombride(name) values('6ft 2in - 187cm');
insert into heightgroombride(name) values('6ft 3in - 190cm');
insert into heightgroombride(name) values('6ft 4in - 193cm');
insert into heightgroombride(name) values('6ft 5in - 195cm');
insert into heightgroombride(name) values('6ft 6in - 198cm');
insert into heightgroombride(name) values('6ft 7in - 200cm');
insert into heightgroombride(name) values('6ft 8in - 203cm');
insert into heightgroombride(name) values('6ft 9in - 205cm');
insert into heightgroombride(name) values('6ft 10in - 208cm');
insert into heightgroombride(name) values('6ft 11in - 210cm');
insert into heightgroombride(name) values('7ft - 213cm');


CREATE TABLE IF NOT EXISTS occupation (
  id int(5) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

insert into occupation(name) values('IT Manager');
insert into occupation(name) values('Software Engineer');
insert into occupation(name) values('Doctor');
insert into occupation(name) values('Advocate');
insert into occupation(name) values('Director/AGM');
insert into occupation(name) values('Teacher');
insert into occupation(name) values('Lecturer');
insert into occupation(name) values('Professore');
insert into occupation(name) values('Constable/Sipai');
insert into occupation(name) values('Sub inspector/inspector');
insert into occupation(name) values('Chairman');
insert into occupation(name) values('Correspondant');
insert into occupation(name) values('Farmer');
insert into occupation(name) values('Supervisor/Floor Incharge');
insert into occupation(name) values('Other');

CREATE TABLE IF NOT EXISTS mothertongue (
  id int(5) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

insert into mothertongue(name) values('Bengali');
insert into mothertongue(name) values('French');
insert into mothertongue(name) values('Telugu');
insert into mothertongue(name) values('Hindi');
insert into mothertongue(name) values('Bihari');
insert into mothertongue(name) values('Koshali');
insert into mothertongue(name) values('Khasi');
insert into mothertongue(name) values('Tamil');
insert into mothertongue(name) values('Rajsthani');
insert into mothertongue(name) values('Haryanvi');
insert into mothertongue(name) values('Urdu');
insert into mothertongue(name) values('Punjabi');
insert into mothertongue(name) values('Other');


CREATE TABLE IF NOT EXISTS annualincome (
  id int(5) NOT NULL AUTO_INCREMENT,
  income varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

insert into annualincome(income) values('< 500000');
insert into annualincome(income) values('500000 - 800000');
insert into annualincome(income) values('800000 - 1000000');
insert into annualincome(income) values('1000000 - 120000');
insert into annualincome(income) values('1200000 - 1400000');
insert into annualincome(income) values('1500000 - 1800000');
insert into annualincome(income) values('1800000 - 2200000');
insert into annualincome(income) values('2200000 - 2500000');
insert into annualincome(income) values('3000000 - 3500000');
insert into annualincome(income) values('3500000 - 4000000');
insert into annualincome(income) values('4000000 > 5000000');
insert into annualincome(income) values('5000000 > 10000000');



CREATE TABLE IF NOT EXISTS user_profile_image (
  id int(5) NOT NULL AUTO_INCREMENT,
  name varchar(500) DEFAULT NULL,
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT 'refers to users table',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;









